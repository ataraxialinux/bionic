define get-build-dir
	out/product/target/build_$(ARCH)
endef

define get-module-dir
	$(call get-build-dir)/$(my_final_local_module)
endef

define get-intermediates-dir
	$(call get-module-dir)/objs
endef

define include-depfile
$(eval $(2) : .KATI_DEPFILE := $1)
endef

define include-depfiles-for-objs
$(foreach obj, $(1), $(call include-depfile, $(obj:%.o=%.d), $(obj)))
endef

define track-src-file-obj
$(eval $(call _track-src-file-obj,$(1)))
endef

define _track-src-file-obj
i := w
$(foreach s,$(1),
my_tracked_src_files += $(s)
my_src_file_obj_$(s) := $$(word $$(words $$(i)),$$(2))
i += w)
endef

define clean-path
$(strip \
	$(if $(call streq,$(words $(1)),1),
	$(eval _rooted := $(filter /%,$(1)))
	$(eval _expanded_path := $(filter-out .,$(subst /,$(space),$(1))))
	$(eval _path := $(if $(_rooted),/)$(subst $(space),/,$(call _clean-path-expanded,$(_rooted),$(_expanded_path))))
	$(if $(_path),
		$(_path),
		.
	)
	,
	$(if $(call streq,$(words $(1)),0),
			.,
			$(error Call clean-path with only one path (without spaces))
		)
	)
)
endef

define get-asm-compiler-args
	-c \
	$(platformInitialCflags) \
	$(commonGlobalCflags) \
	$(deviceGlobalCflags) \
	$(extraExternalCflags) \
	$(platformCflags) \
	-target $(clangTarget) \
	$(PRIVATE_ASFLAGS) \
	-fPIC \
	$(commonGlobalCppflags) \
	$(deviceGlobalCppflags) \
	$(PRIVATE_CPPFLAGS) \
	-D__ASSEMBLY__ \
	$(includeCflags) \
	$(PRIVATE_C_INCLUDES) \
	$(noOverrideGlobalCflags) \
	$(if $(PRIVATE_NO_STL_STD),,-std=gnu99)
endef

define get-c-compiler-args
	-c \
	$(platformInitialCflags) \
	$(commonGlobalCflags) \
	$(deviceGlobalCflags) \
	$(extraExternalCflags) \
	$(platformCflags) \
	-target $(clangTarget) \
	$(PRIVATE_CFLAGS) \
	-fPIC \
	$(PRIVATE_C_ONLY_FLAGS) \
	$(commonGlobalCppflags) \
	$(deviceGlobalCppflags) \
	$(PRIVATE_CPPFLAGS) \
	$(includeCflags) \
	$(PRIVATE_C_INCLUDES) \
	$(noOverrideGlobalCflags) \
	$(if $(PRIVATE_NO_STL_STD),,-std=gnu99)
endef

define get-cpp-compiler-args
	-c \
	$(platformInitialCflags) \
	$(commonGlobalCflags) \
	$(deviceGlobalCflags) \
	$(extraExternalCflags) \
	$(platformCflags) \
	-target $(clangTarget) \
	$(PRIVATE_CFLAGS) \
	$(PRIVATE_RTTI_FLAG) \
	-fPIC \
	$(commonGlobalCppflags) \
	$(deviceGlobalCppflags) \
	$(PRIVATE_CPPFLAGS) \
	$(includeCflags) \
	$(PRIVATE_C_INCLUDES) \
	$(noOverrideGlobalCflags) \
	$(if $(PRIVATE_NO_STL_STD),,-std=gnu++17) \
	$(PRIVATE_CXX_STL_VERSION)
endef

define transform-s-to-o
@echo "asm: $(PRIVATE_MODULE) <= $<"
@mkdir -p $(dir $@)
@$(CC) \
	$(call get-asm-compiler-args) \
	-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<
endef

define transform-c-to-o
@echo "C: $(PRIVATE_MODULE) <= $<"
@mkdir -p $(dir $@)
@$(CC) \
	$(get-c-compiler-args) \
	-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<
endef

define transform-cpp-to-o
@echo "C++: $(PRIVATE_MODULE) <= $<"
@mkdir -p $(dir $@)
@$(CXX) \
	$(get-cpp-compiler-args) \
	-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<
endef

define transform-o-to-shared-lib-inner
@$(CXX) \
	-nostdlib -Wl,-soname,$(notdir $@) \
	-Wl,--gc-sections \
	-shared \
	$(PRIVATE_TARGET_CRTBEGIN_SO_O) \
	$(PRIVATE_ALL_OBJECTS) \
	-Wl,--whole-archive \
	$(PRIVATE_ALL_WHOLE_STATIC_LIBRARIES) \
	-Wl,--no-whole-archive \
	$(if $(PRIVATE_GROUP_STATIC_LIBRARIES),-Wl$(comma)--start-group) \
	$(PRIVATE_ALL_STATIC_LIBRARIES) \
	$(if $(PRIVATE_GROUP_STATIC_LIBRARIES),-Wl$(comma)--end-group) \
	$(PRIVATE_TARGET_LIBCRT_BUILTINS) \
	$(deviceGlobalLdflags) \
	-target $(clangTarget) \
	$(platformLdflags) \
	$(PRIVATE_LDFLAGS) \
	$(PRIVATE_ALL_SHARED_LIBRARIES) \
	-o $@ \
	$(PRIVATE_TARGET_CRTEND_SO_O) \
	$(PRIVATE_LDLIBS)
endef

define transform-o-to-shared-lib
@echo "SharedLib: $(PRIVATE_MODULE) ($@)"
@mkdir -p $(dir $@)
$(transform-o-to-shared-lib-inner)
@cp $@ $(builddir)/$(notdir $@)
endef

define transform-o-to-static-lib
@echo "StaticLib: $(PRIVATE_MODULE) ($@)"
@mkdir -p $(dir $@)
@sh scripts/whole-static-libs.sh $(AR) $@ $(shell pwd) "$(PRIVATE_INTERMEDIATES)" "$(PRIVATE_ALL_WHOLE_STATIC_LIBRARIES)"
@cp $@ $(builddir)/$(notdir $@)
endef

define transform-o-to-executable-inner
@$(CXX) -pie \
	-nostdlib -Bdynamic \
	-Wl,-dynamic-linker,$(dynamic_linker) \
	-Wl,--gc-sections \
	-Wl,-z,nocopyreloc \
	$(PRIVATE_TARGET_CRTBEGIN_DYNAMIC_O) \
	$(PRIVATE_ALL_OBJECTS) \
	-Wl,--whole-archive \
	$(PRIVATE_ALL_WHOLE_STATIC_LIBRARIES) \
	-Wl,--no-whole-archive \
	$(if $(PRIVATE_GROUP_STATIC_LIBRARIES),-Wl$(comma)--start-group) \
	$(PRIVATE_ALL_STATIC_LIBRARIES) \
	$(if $(PRIVATE_GROUP_STATIC_LIBRARIES),-Wl$(comma)--end-group) \
	$(if $(filter true,$(NATIVE_COVERAGE)),$(PRIVATE_TARGET_COVERAGE_LIB)) \
	$(PRIVATE_TARGET_LIBCRT_BUILTINS) \
	$(deviceGlobalLdflags) \
	-target $(clangTarget) \
	$(platformLdflags) \
	$(PRIVATE_LDFLAGS) \
	$(PRIVATE_ALL_SHARED_LIBRARIES) \
	-o $@ \
	$(PRIVATE_TARGET_CRTEND_O) \
	$(PRIVATE_LDLIBS)
endef

define transform-o-to-executable
@echo "Executable: $(PRIVATE_MODULE) ($@)"
@mkdir -p $(dir $@)
$(transform-o-to-executable-inner)
@cp $@ $(builddir)/$(notdir $@)
endef

define transform-o-to-static-executable-inner
@$(CXX) \
	-nostdlib -Bstatic \
	$(if $(filter $(PRIVATE_LDFLAGS),-shared),,-static) \
	-Wl,--gc-sections \
	-o $@ \
	$(PRIVATE_TARGET_CRTBEGIN_STATIC_O) \
	$(deviceGlobalLdflags) \
	-target $(clangTarget) \
	$(platformLdflags) \
	$(PRIVATE_LDFLAGS) \
	$(PRIVATE_ALL_OBJECTS) \
	-Wl,--whole-archive \
	$(PRIVATE_ALL_WHOLE_STATIC_LIBRARIES) \
	-Wl,--no-whole-archive \
	$(filter-out %libcompiler_rt.hwasan.a %libc_nomalloc.hwasan.a %libc.hwasan.a %libcompiler_rt.a %libc_nomalloc.a %libc.a,$(PRIVATE_ALL_STATIC_LIBRARIES)) \
	-Wl,--start-group \
	$(filter %libc.a %libc.hwasan.a,$(PRIVATE_ALL_STATIC_LIBRARIES)) \
	$(filter %libc_nomalloc.a %libc_nomalloc.hwasan.a,$(PRIVATE_ALL_STATIC_LIBRARIES)) \
	$(if $(filter true,$(NATIVE_COVERAGE)),$(PRIVATE_TARGET_COVERAGE_LIB)) \
	$(filter %libcompiler_rt.a %libcompiler_rt.hwasan.a,$(PRIVATE_ALL_STATIC_LIBRARIES)) \
	$(PRIVATE_TARGET_LIBCRT_BUILTINS) \
	-Wl,--end-group \
	$(PRIVATE_TARGET_CRTEND_O)
endef

define transform-o-to-static-executable
@echo "StaticExecutable: $(PRIVATE_MODULE) ($@)"
@mkdir -p $(dir $@)
$(transform-o-to-static-executable-inner)
@cp $@ $(builddir)/$(notdir $@)
endef

define transform-txt-to-map
@echo "STUB: $(PRIVATE_MODULE) ($@)"
@mkdir -p $(dir $@)
@python3 bionic/tools/generate-version-script.py $(ARCH) $< $@
endef
