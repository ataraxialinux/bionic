include defines.mk

include cleanvars.mk

LOCAL_MODULE := libziparchive
LOCAL_SRC_FILES := \
	system/libziparchive/zip_archive.cc \
	system/libziparchive/zip_archive_stream_entry.cc \
	system/libziparchive/zip_cd_entry_map.cc \
	system/libziparchive/zip_error.cpp \
	system/libziparchive/zip_writer.cc
LOCAL_CFLAGS := \
	-DZLIB_CONST \
	-Werror \
	-Wall \
	-D_FILE_OFFSET_BITS=64 \
	-DINCFS_SUPPORT_DISABLED=1
LOCAL_CPPFLAGS := \
	-Wno-missing-field-initializers \
	-Wconversion \
	-Wno-sign-conversion
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/zlib \
	-Isystem/libbase/include \
	-Iexternal/googletest/googletest/include \
	-Isystem/libziparchive/include \
	-Isystem/libziparchive/incfs_support/include
LOCAL_CXX_STL_VERSION := -std=c++2a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libziparchive
LOCAL_EXTRA_DEPS := \
	$(builddir)/libziparchive.a \
	$(builddir)/libbase.so \
	$(builddir)/liblog.so \
	$(builddir)/libz.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/liblog.so \
	$(builddir)/libz.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libziparchive.a

include shared_library.mk
