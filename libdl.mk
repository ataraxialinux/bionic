include defines.mk

include cleanvars.mk

LOCAL_MODULE := ld-android
LOCAL_SRC_FILES := bionic/linker/ld_android.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror
LOCAL_C_INCLUDES := $(bionicHeaders)
LOCAL_LDFLAGS := \
	-Wl,--exclude-libs=libgcc.a \
	-Wl,--exclude-libs=libgcc_stripped.a \
	-Wl,--exclude-libs=libclang_rt.builtins-arm-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-aarch64-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-i686-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-x86_64-android.a
LOCAL_LDFLAGS_x86 := -Wl,--exclude-libs=libgcc_eh.a
LOCAL_LDFLAGS_x86_64 := -Wl,--exclude-libs=libgcc_eh.a
LOCAL_USE_CRT := false

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libdl_static
LOCAL_SRC_FILES := \
	bionic/libdl/libdl.cpp \
	bionic/libdl/libdl_cfi.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Ibionic/libc

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libdl_android
LOCAL_SRC_FILES := bionic/libdl/libdl_android.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror
LOCAL_C_INCLUDES := $(bionicHeaders)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libdl_android
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libdl_android.a
LOCAL_LDFLAGS := \
	-Wl,--exclude-libs=libgcc.a \
	-Wl,--exclude-libs=libgcc_stripped.a \
	-Wl,--exclude-libs=libclang_rt.builtins-arm-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-aarch64-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-i686-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-x86_64-android.a
LOCAL_LDFLAGS_x86 := -Wl,--exclude-libs=libgcc_eh.a
LOCAL_LDFLAGS_x86_64 := -Wl,--exclude-libs=libgcc_eh.a
LOCAL_EXTRA_DEPS := $(builddir)/ld-android.so $(builddir)/libdl_android.a
LOCAL_LDLIBS := $(builddir)/ld-android.so
LOCAL_USE_CRT := false

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libdl
LOCAL_SRC_FILES := bionic/libdl/libdl_static.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror
LOCAL_C_INCLUDES := $(bionicHeaders)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libdl
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libdl_static.a
LOCAL_LDFLAGS := \
	-Wl,--version-script,$(builddir)/libdl_shared/objs/bionic/libdl/libdl.map \
	-Wl,--exclude-libs=libgcc.a \
	-Wl,--exclude-libs=libgcc_stripped.a \
	-Wl,--exclude-libs=libclang_rt.builtins-arm-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-aarch64-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-i686-android.a \
	-Wl,--exclude-libs=libclang_rt.builtins-x86_64-android.a
LOCAL_LDFLAGS_arm := -Wl,--hash-style=both
LOCAL_LDFLAGS_x86 := \
	-Wl,--exclude-libs=libgcc_eh.a \
	-Wl,--hash-style=both
LOCAL_LDFLAGS_x86_64 := -Wl,--exclude-libs=libgcc_eh.a
LOCAL_EXTRA_DEPS := $(builddir)/ld-android.so
LOCAL_LDLIBS := $(builddir)/ld-android.so
LOCAL_API_STUBS := bionic/libdl/libdl.map.txt
LOCAL_USE_CRT := false

include shared_library.mk
