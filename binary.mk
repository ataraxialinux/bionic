include base.mk

ifeq ($(LOCAL_SYSTEM_SHARED_LIBRARIES),none)
	my_system_shared_libraries := libc libm libdl
else
	my_system_shared_libraries := $(LOCAL_SYSTEM_SHARED_LIBRARIES)
	my_system_shared_libraries := $(patsubst libc,libc libdl,$(my_system_shared_libraries))
endif

my_module := $(LOCAL_MODULE)
my_asflags := $(LOCAL_ASFLAGS)
my_cflags := $(LOCAL_CFLAGS)
my_cppflags := $(LOCAL_CPPFLAGS)
my_c_includes := $(LOCAL_C_INCLUDES)
my_c_only_flags := $(LOCAL_C_ONLY_FLAGS)
my_src_files := $(LOCAL_SRC_FILES)
my_src_files_exclude := $(LOCAL_SRC_FILES_EXCLUDE)
my_shared_libraries := $(filter-out $(my_system_shared_libraries),$(LOCAL_SHARED_LIBRARIES))
my_static_libraries := $(LOCAL_STATIC_LIBRARIES)
my_whole_static_libraries := $(LOCAL_WHOLE_STATIC_LIBRARIES)
my_ldflags := $(LOCAL_LDFLAGS)
my_ldlibs := $(LOCAL_LDLIBS)
my_stubs := $(LOCAL_API_STUBS)

my_src_files += $(LOCAL_SRC_FILES_$(ARCH)) $(LOCAL_SRC_FILES_$(my_32_64_bit_suffix))
my_src_files_exclude += $(LOCAL_SRC_FILES_EXCLUDE_$(ARCH)) $(LOCAL_SRC_FILES_EXCLUDE_$(my_32_64_bit_suffix))
my_shared_libraries += $(LOCAL_SHARED_LIBRARIES_$(ARCH)) $(LOCAL_SHARED_LIBRARIES_$(my_32_64_bit_suffix))
my_static_libraries := $(LOCAL_STATIC_LIBRARIES_$(ARCH)) $(LOCAL_STATIC_LIBRARIES_$(my_32_64_bit_suffix)) $(my_static_libraries)
my_whole_static_libraries := $(LOCAL_WHOLE_STATIC_LIBRARIES_$(ARCH)) $(LOCAL_WHOLE_STATIC_LIBRARIES_$(my_32_64_bit_suffix)) $(my_whole_static_libraries)
my_cflags += $(LOCAL_CFLAGS_$(ARCH)) $(LOCAL_CFLAGS_$(my_32_64_bit_suffix))
my_cppflags += $(LOCAL_CPPFLAGS_$(ARCH)) $(LOCAL_CPPFLAGS_$(my_32_64_bit_suffix))
my_asflags += $(LOCAL_ASFLAGS_$(ARCH)) $(LOCAL_ASFLAGS_$(my_32_64_bit_suffix))
my_c_includes += $(LOCAL_C_INCLUDES_$(ARCH)) $(LOCAL_C_INCLUDES_$(my_32_64_bit_suffix))
my_c_only_flags += $(LOCAL_C_ONLY_FLAGS_$(ARCH)) $(LOCAL_C_ONLY_FLAGS_$(my_32_64_bit_suffix))
my_ldflags += $(LOCAL_LDFLAGS_$(ARCH)) $(LOCAL_LDFLAGS_$(my_32_64_bit_suffix))

my_missing_exclude_files := $(filter-out $(my_src_files),$(my_src_files_exclude))
ifneq ($(my_missing_exclude_files),)
$(warning Files are listed in LOCAL_SRC_FILES_EXCLUDE but not LOCAL_SRC_FILES)
$(error $(my_missing_exclude_files))
endif
my_src_files := $(filter-out $(my_src_files_exclude),$(my_src_files))

my_src_files := $(patsubst /%,%,$(my_src_files))

ifeq ($(strip $(LOCAL_RTTI_FLAG)),)
LOCAL_RTTI_FLAG := -fno-rtti
endif

stub_sources := $(filter-out ../%,$(my_stubs))
stub_sources := $(addprefix $(intermediates)/,$(stub_sources:.txt=))

ifneq ($(strip $(stub_sources)),)
$(stub_sources): $(intermediates)/%: $(TOPDIR)$(LOCAL_PATH)/%.txt
	$(transform-txt-to-map)
$(call include-depfiles-for-objs, $(stub_sources))
endif

asm_sources_S := $(filter %.S,$(my_src_files))
asm_sources_S := $(filter-out ../%,$(asm_sources_S))
asm_objects_S := $(addprefix $(intermediates)/,$(asm_sources_S:.S=.o))
$(call track-src-file-obj,$(asm_sources_S),$(asm_objects_S))

ifneq ($(strip $(asm_objects_S)),)
$(asm_objects_S): $(intermediates)/%.o: $(TOPDIR)$(LOCAL_PATH)/%.S
	$(transform-s-to-o)
$(call include-depfiles-for-objs, $(asm_objects_S))
endif

asm_sources_s := $(filter %.s,$(my_src_files))
asm_sources_s := $(filter-out ../%,$(asm_sources_s))
asm_objects_s := $(addprefix $(intermediates)/,$(asm_sources_s:.s=.o))
$(call track-src-file-obj,$(asm_sources_s),$(asm_objects_s))

ifneq ($(strip $(asm_objects_s)),)
$(asm_objects_s): $(intermediates)/%.o: $(TOPDIR)$(LOCAL_PATH)/%.s
	$(transform-s-to-o)
endif

asm_objects := $(asm_objects_S) $(asm_objects_s)

c_normal_sources := $(filter-out ../%,$(filter %.c,$(my_src_files)))
c_objects := $(addprefix $(intermediates)/,$(c_normal_sources:.c=.o))
$(call track-src-file-obj,$(c_normal_sources),$(c_objects))

ifneq ($(strip $(c_objects)),)
$(c_objects): $(intermediates)/%.o: $(TOPDIR)$(LOCAL_PATH)/%.c
	$(transform-c-to-o)
$(call include-depfiles-for-objs, $(c_objects))
endif

cpp_normal_sources := $(filter-out ../%,$(filter %.cpp,$(my_src_files)))
cpp_objects := $(addprefix $(intermediates)/,$(cpp_normal_sources:.cpp=.o))
$(call track-src-file-obj,$(cpp_normal_sources),$(cpp_objects))

ifneq ($(strip $(cpp_objects)),)
$(cpp_objects): $(intermediates)/%.o: \
	$(TOPDIR)$(LOCAL_PATH)/%.cpp
	$(transform-cpp-to-o)
$(call include-depfiles-for-objs, $(cpp_objects))
endif

cc_normal_sources := $(filter-out ../%,$(filter %.cc,$(my_src_files)))
cc_objects := $(addprefix $(intermediates)/,$(cc_normal_sources:.cc=.o))
$(call track-src-file-obj,$(cc_normal_sources),$(cc_objects))

ifneq ($(strip $(cc_objects)),)
$(cc_objects): $(intermediates)/%.o: \
	$(TOPDIR)$(LOCAL_PATH)/%.cc
	$(transform-cpp-to-o)
$(call include-depfiles-for-objs, $(cc_objects))
endif

normal_objects := \
	$(asm_objects) \
	$(cc_objects) \
	$(cpp_objects) \
	$(gen_cpp_objects) \
	$(gen_asm_objects) \
	$(c_objects) \
	$(gen_c_objects)

all_objects := $(normal_objects)

LOCAL_INTERMEDIATE_TARGETS += $(all_objects)

$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_MODULE := $(my_module)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_ALL_SHARED_LIBRARIES := $(my_shared_libraries)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_ALL_STATIC_LIBRARIES := $(my_static_libraries)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_ALL_WHOLE_STATIC_LIBRARIES := $(my_whole_static_libraries)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_ALL_OBJECTS := $(strip $(all_objects))
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_STUBS := $(strip $(stub_sources))
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_ASFLAGS := $(my_asflags)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_CFLAGS := $(my_cflags)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_CPPFLAGS := $(my_cppflags)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_RTTI_FLAG := $(LOCAL_RTTI_FLAG)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_C_INCLUDES := $(my_c_includes)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_C_ONLY_FLAGS := $(my_c_only_flags)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_LDFLAGS := $(my_ldflags)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_LDLIBS := $(my_ldlibs)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_API_STUBS := $(my_stubs)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_NO_STL_STD := $(LOCAL_MODULE_NO_STL_STD)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_CXX_STL_VERSION := $(LOCAL_CXX_STL_VERSION)
$(LOCAL_INTERMEDIATE_TARGETS): PRIVATE_INTERMEDIATES := $(strip $(intermediates))

