include funcs.mk
include defines.mk

LOCAL_MODULE := crt
PRIVATE_MODULE := crt

builddir := $(call get-build-dir)
moduleddir := $(call get-module-dir)
intermediates := $(call get-intermediates-dir)

crts := \
	$(builddir)/crtbegin_dynamic.o \
	$(builddir)/crtbegin_so.o \
	$(builddir)/crtbegin_static.o \
	$(builddir)/crtend_android.o \
	$(builddir)/crtend_so.o

crt_so_flags :=

ifeq ($(ARCH),x86)
crt_so_flags += -fPIC
endif

ifeq ($(ARCH),x86_64)
crt_so_flags += -fPIC
endif

all: $(crts)

$(builddir)/crtbrand.o: bionic/libc/arch-common/bionic/crtbrand.S
	@echo "asm: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(call get-asm-compiler-args) \
		-DPLATFORM_SDK_VERSION=$(AndroidApiLevel) \
		$(crt_so_flags) \
		-Ibionic/libc \
		-Ibionic/libc/include \
		-Ibionic/libc/private \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<

$(builddir)/crtbegin_so1.o: bionic/libc/arch-common/bionic/crtbegin_so.c
	@echo "C: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(get-c-compiler-args) \
		$(crt_so_flags) \
		-Ibionic/libc/include \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<

$(builddir)/crtend_so1.o: bionic/libc/arch-common/bionic/crtend_so.S
	@echo "asm: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(call get-asm-compiler-args) \
		$(crt_so_flags) \
		-Ibionic/libc \
		-Ibionic/libc/include \
		-Ibionic/libc/private \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<

$(builddir)/crtbegin_so.o: $(builddir)/crtbrand.o $(builddir)/crtbegin_so1.o
	@echo "SharedLib: crt ($@)"
	@$(LD) -r -o $@ $^

$(builddir)/crtend_so.o: $(builddir)/crtbrand.o $(builddir)/crtend_so1.o
	@echo "SharedLib: crt ($@)"
	@$(LD) -r -o $@ $^

$(builddir)/crtbegin_dynamic1.o: bionic/libc/arch-common/bionic/crtbegin.c
	@echo "C: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(get-c-compiler-args) \
		$(bionicHeaders) \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<

$(builddir)/crtbegin_static1.o: bionic/libc/arch-common/bionic/crtbegin.c
	@echo "C: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(get-c-compiler-args) \
		$(bionicHeaders) \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<

$(builddir)/crtbegin_dynamic.o: $(builddir)/crtbrand.o $(builddir)/crtbegin_dynamic1.o
	@echo "SharedLib: crt ($@)"
	@$(LD) -r -o $@ $^

$(builddir)/crtbegin_static.o: $(builddir)/crtbrand.o $(builddir)/crtbegin_static1.o
	@echo "SharedLib: crt ($@)"
	@$(LD) -r -o $@ $^

$(builddir)/crtend_android.o: bionic/libc/arch-common/bionic/crtend.S
	@echo "asm: crt <= $<"
	@mkdir -p $(dir $@)
	@$(CC) \
		$(call get-asm-compiler-args) \
		-Ibionic/libc \
		-Ibionic/libc/include \
		-Ibionic/libc/private \
		-MD -MF $(patsubst %.o,%.d,$@) -o $@ $<
