include defines.mk

include cleanvars.mk

LOCAL_MODULE := libjsoncpp
LOCAL_SRC_FILES := \
	external/jsoncpp/src/lib_json/json_reader.cpp \
	external/jsoncpp/src/lib_json/json_value.cpp \
	external/jsoncpp/src/lib_json/json_writer.cpp
LOCAL_CFLAGS := \
	-DJSON_USE_EXCEPTION=0 \
	-DJSONCPP_NO_LOCALE_SUPPORT \
	-Wall \
	-Werror \
	-Wno-implicit-fallthrough
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/jsoncpp/include \
	-Iexternal/jsoncpp/src/lib_json

include static_library.mk
