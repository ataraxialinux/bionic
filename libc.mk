include defines.mk

libc_common_src_files = \
	bionic/libc/bionic/ether_aton.c \
	bionic/libc/bionic/ether_ntoa.c \
	bionic/libc/bionic/exit.cpp \
	bionic/libc/bionic/initgroups.c \
	bionic/libc/bionic/isatty.c \
	bionic/libc/bionic/sched_cpualloc.c \
	bionic/libc/bionic/sched_cpucount.c \
	bionic/libc/stdio/fmemopen.cpp \
	bionic/libc/stdio/parsefloat.c \
	bionic/libc/stdio/refill.c \
	bionic/libc/stdio/stdio.cpp \
	bionic/libc/stdio/stdio_ext.cpp \
	bionic/libc/stdio/vfscanf.cpp \
	bionic/libc/stdio/vfwscanf.c

libc_common_src_files_32 = \
	bionic/libc/bionic/legacy_32_bit_support.cpp \
	bionic/libc/bionic/time64.c

libc_common_flags = \
	-D_LIBC=1 \
	-D__BIONIC_LP32_USE_STAT64 \
	-Wall \
	-Wextra \
	-Wunused \
	-Wno-char-subscripts \
	-Wno-deprecated-declarations \
	-Wno-gcc-compat \
	-Wframe-larger-than=2048 \
	-Werror=pointer-to-int-cast \
	-Werror=int-to-pointer-cast \
	-Werror=type-limits \
	-Werror \
	-Wexit-time-destructors \
	-fno-emulated-tls \
	-fno-builtin

libc_common_flags += \
	-DSCUDO_ZERO_CONTENTS \
	-DSCUDO_PATTERN_FILL_CONTENTS \
	-DUSE_SCUDO

libc_conlyflags = -std=gnu99

libc_include_dirs = \
	$(bionicHeaders) \
	-Ibionic/libc \
	-Ibionic/libc/async_safe/include \
	-Ibionic/libc/platform \
	-Isystem/core/libcutils/include \
	-Iexternal/gwp_asan \
	-Isystem/logging/liblog/include

libc_ldflags = -Wl,-z,muldefs

include cleanvars.mk

LOCAL_MODULE := libc_bootstrap
LOCAL_SRC_FILES := \
	bionic/libc/bionic/__libc_init_main_thread.cpp \
	bionic/libc/bionic/__stack_chk_fail.cpp \
	bionic/libc/bionic/bionic_call_ifunc_resolver.cpp \
	bionic/libc/bionic/getauxval.cpp
LOCAL_SRC_FILES_arm64 := \
	bionic/libc/arch-arm64/bionic/__set_tls.c
LOCAL_SRC_FILES_x86 := \
	bionic/libc/arch-x86/bionic/__libc_init_sysinfo.cpp \
	bionic/libc/arch-x86/bionic/__libc_int0x80.S \
	bionic/libc/arch-x86/bionic/__set_tls.cpp
LOCAL_SRC_FILES_x86_64 := \
	bionic/libc/arch-x86_64/bionic/__set_tls.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-fno-stack-protector \
	-ffreestanding
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_init_static
LOCAL_SRC_FILES := bionic/libc/bionic/libc_init_static.cpp
LOCAL_CFLAGS := $(libc_common_flags) \
	-fno-stack-protector \
	-ffreestanding
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_init_dynamic
LOCAL_SRC_FILES := bionic/libc/bionic/libc_init_dynamic.cpp
LOCAL_CFLAGS := $(libc_common_flags) \
	-fno-stack-protector
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_tzcode
LOCAL_SRC_FILES := \
	bionic/libc/tzcode/asctime.c \
	bionic/libc/tzcode/difftime.c \
	bionic/libc/tzcode/localtime.c \
	bionic/libc/tzcode/strftime.c \
	bionic/libc/tzcode/strptime.c \
	bionic/libc/tzcode/bionic.cpp \
	bionic/libc/upstream-openbsd/lib/libc/time/wcsftime.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-unused-parameter \
	-DALL_STATE \
	-DSTD_INSPIRED \
	-DTHREAD_SAFE \
	-DTM_GMTOFF=tm_gmtoff \
	-DTZDIR=\"/system/usr/share/zoneinfo\" \
	-DHAVE_POSIX_DECLS=0 \
	-DUSG_COMPAT=1 \
	-DWILDABBR=\"\" \
	-DNO_RUN_TIME_WARNINGS_ABOUT_YEAR_2000_PROBLEMS_THANK_YOU \
	-Dlint \
	-Wno-unused-function
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/tzcode
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_dns
LOCAL_SRC_FILES := \
	bionic/libc/dns/nameser/ns_name.c \
	bionic/libc/dns/nameser/ns_netint.c \
	bionic/libc/dns/nameser/ns_parse.c \
	bionic/libc/dns/nameser/ns_print.c \
	bionic/libc/dns/nameser/ns_samedomain.c \
	bionic/libc/dns/nameser/ns_ttl.c \
	bionic/libc/dns/net/getaddrinfo.c \
	bionic/libc/dns/net/gethnamaddr.c \
	bionic/libc/dns/net/getnameinfo.c \
	bionic/libc/dns/net/getservent.c \
	bionic/libc/dns/net/nsdispatch.c \
	bionic/libc/dns/net/sethostent.c \
	bionic/libc/dns/resolv/herror.c \
	bionic/libc/dns/resolv/res_cache.c \
	bionic/libc/dns/resolv/res_comp.c \
	bionic/libc/dns/resolv/res_data.cpp \
	bionic/libc/dns/resolv/res_debug.c \
	bionic/libc/dns/resolv/res_init.c \
	bionic/libc/dns/resolv/res_mkquery.c \
	bionic/libc/dns/resolv/res_query.c \
	bionic/libc/dns/resolv/res_send.c \
	bionic/libc/dns/resolv/res_state.c \
	bionic/libc/dns/resolv/res_stats.c \
	bionic/libc/upstream-netbsd/lib/libc/isc/ev_streams.c \
	bionic/libc/upstream-netbsd/lib/libc/isc/ev_timers.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-DANDROID_CHANGES \
	-DINET6 \
	-Wno-unused-parameter \
	-include netbsd-compat.h \
	-Wframe-larger-than=66000 \
	-include private/bsd_sys_param.h \
	-Wno-unused-function
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/dns/include \
	-Ibionic/libc/private \
	-Ibionic/libc/upstream-netbsd/lib/libc/include \
	-Ibionic/libc/upstream-netbsd/android/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_freebsd
LOCAL_SRC_FILES := \
	bionic/libc/upstream-freebsd/lib/libc/gen/ldexp.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/getopt_long.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/hcreate.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/hcreate_r.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/hdestroy_r.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/hsearch_r.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/qsort.c \
	bionic/libc/upstream-freebsd/lib/libc/stdlib/quick_exit.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcpcpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcpncpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscasecmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscat.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcschr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscspn.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsdup.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcslcat.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcslen.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsncasecmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsncat.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsncmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsncpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsnlen.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcspbrk.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsrchr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsspn.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsstr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcstok.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemchr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemcmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemcpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemmove.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemset.c
LOCAL_SRC_FILES_EXCLUDE_arm64 := bionic/libc/upstream-freebsd/lib/libc/string/wmemmove.c
LOCAL_SRC_FILES_EXCLUDE_x86 := \
	bionic/libc/upstream-freebsd/lib/libc/string/wcschr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcslen.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcsrchr.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemcmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscat.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wcscpy.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemcmp.c \
	bionic/libc/upstream-freebsd/lib/libc/string/wmemset.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-Wno-unused-parameter \
	-include freebsd-compat.h
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/upstream-freebsd/android/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_freebsd_large_stack
LOCAL_SRC_FILES := bionic/libc/upstream-freebsd/lib/libc/gen/glob.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-include freebsd-compat.h \
	-Wframe-larger-than=66000
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/upstream-freebsd/android/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_netbsd
LOCAL_SRC_FILES := \
	bionic/libc/upstream-netbsd/common/lib/libc/stdlib/random.c \
	bionic/libc/upstream-netbsd/lib/libc/gen/nice.c \
	bionic/libc/upstream-netbsd/lib/libc/gen/psignal.c \
	bionic/libc/upstream-netbsd/lib/libc/gen/utime.c \
	bionic/libc/upstream-netbsd/lib/libc/inet/nsap_addr.c \
	bionic/libc/upstream-netbsd/lib/libc/regex/regcomp.c \
	bionic/libc/upstream-netbsd/lib/libc/regex/regerror.c \
	bionic/libc/upstream-netbsd/lib/libc/regex/regexec.c \
	bionic/libc/upstream-netbsd/lib/libc/regex/regfree.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/bsearch.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/drand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/erand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/jrand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/lcong48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/lrand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/mrand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/nrand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/_rand48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/rand_r.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/reallocarr.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/seed48.c \
	bionic/libc/upstream-netbsd/lib/libc/stdlib/srand48.c
LOCAL_SRC_FILES_32 := bionic/libc/upstream-netbsd/common/lib/libc/hash/sha1/sha1.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-Wno-unused-parameter \
	-DPOSIX_MISTAKE \
	-include netbsd-compat.h \
	-Wno-unused-function
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/upstream-netbsd/android/include \
	-Ibionic/libc/upstream-netbsd/lib/libc/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_openbsd_ndk
LOCAL_SRC_FILES := \
	bionic/libc/upstream-openbsd/lib/libc/gen/alarm.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/ctype_.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/daemon.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/err.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/errx.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/fnmatch.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/ftok.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/getprogname.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/setprogname.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/verr.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/verrx.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/vwarn.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/vwarnx.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/warn.c \
	bionic/libc/upstream-openbsd/lib/libc/gen/warnx.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/btowc.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/mbrlen.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/mbstowcs.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/mbtowc.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcscoll.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstoimax.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstol.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstoll.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstombs.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstoul.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstoull.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcstoumax.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wcsxfrm.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wctob.c \
	bionic/libc/upstream-openbsd/lib/libc/locale/wctomb.c \
	bionic/libc/upstream-openbsd/lib/libc/net/base64.c \
	bionic/libc/upstream-openbsd/lib/libc/net/htonl.c \
	bionic/libc/upstream-openbsd/lib/libc/net/htons.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_lnaof.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_makeaddr.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_netof.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_ntoa.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_ntop.c \
	bionic/libc/upstream-openbsd/lib/libc/net/inet_pton.c \
	bionic/libc/upstream-openbsd/lib/libc/net/ntohl.c \
	bionic/libc/upstream-openbsd/lib/libc/net/ntohs.c \
	bionic/libc/upstream-openbsd/lib/libc/net/res_random.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fgetln.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fgetwc.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fgetws.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/flags.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fpurge.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fputwc.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fputws.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fvwrite.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/fwide.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/getdelim.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/gets.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/makebuf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/mktemp.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/open_memstream.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/open_wmemstream.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/rget.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/setvbuf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/ungetc.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/ungetwc.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/vasprintf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/vdprintf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/vsscanf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/vswprintf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/vswscanf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/wbuf.c \
	bionic/libc/upstream-openbsd/lib/libc/stdio/wsetup.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/abs.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/div.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/getenv.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/getsubopt.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/insque.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/imaxabs.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/imaxdiv.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/labs.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/ldiv.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/llabs.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/lldiv.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/lsearch.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/recallocarray.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/remque.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/setenv.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/tfind.c \
	bionic/libc/upstream-openbsd/lib/libc/stdlib/tsearch.c \
	bionic/libc/upstream-openbsd/lib/libc/string/memccpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcasecmp.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcasestr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcoll.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcspn.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strdup.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strndup.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strpbrk.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strsep.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strspn.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strtok.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strxfrm.c \
	bionic/libc/upstream-openbsd/lib/libc/string/wcslcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/wcswidth.c \
	bionic/libc/bionic/fts.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-Wno-unused-parameter \
	-include openbsd-compat.h \
	-Wno-unused-function \
	-Wno-typedef-redefinition
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/private \
	-Ibionic/libc/stdio \
	-Ibionic/libc/upstream-openbsd/android/include \
	-Ibionic/libc/upstream-openbsd/lib/libc/include \
	-Ibionic/libc/upstream-openbsd/lib/libc/gdtoa
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_openbsd_large_stack
LOCAL_SRC_FILES := \
	bionic/libc/stdio/vfprintf.cpp \
	bionic/libc/stdio/vfwprintf.cpp \
	bionic/libc/upstream-openbsd/lib/libc/string/memmem.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strstr.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-include openbsd-compat.h \
	-Wno-sign-compare \
	-Wframe-larger-than=5000
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/private \
	-Ibionic/libc/upstream-openbsd/android/include \
	-Ibionic/libc/upstream-openbsd/lib/libc/include \
	-Ibionic/libc/upstream-openbsd/lib/libc/gdtoa \
	-Ibionic/libc/upstream-openbsd/lib/libc/stdio
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_openbsd
LOCAL_SRC_FILES := \
	bionic/libc/upstream-openbsd/lib/libc/crypt/arc4random.c \
	bionic/libc/upstream-openbsd/lib/libc/crypt/arc4random_uniform.c \
	bionic/libc/upstream-openbsd/lib/libc/string/memchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/memrchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpncpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strlcat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strlcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncmp.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncpy.c
LOCAL_SRC_FILES_EXCLUDE_arm := \
	bionic/libc/upstream-openbsd/lib/libc/string/strcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcat.c
LOCAL_SRC_FILES_EXCLUDE_arm64 := \
	bionic/libc/upstream-openbsd/lib/libc/string/memchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/memrchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncmp.c
LOCAL_SRC_FILES_EXCLUDE_x86 := \
	bionic/libc/upstream-openbsd/lib/libc/string/memchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/memrchr.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpncpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncmp.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strlcat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strlcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncat.c
LOCAL_SRC_FILES_EXCLUDE_x86_64 := \
	bionic/libc/upstream-openbsd/lib/libc/string/stpcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/stpncpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strcpy.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncat.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncmp.c \
	bionic/libc/upstream-openbsd/lib/libc/string/strncpy.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-Wno-unused-parameter \
	-include openbsd-compat.h
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/private \
	-Ibionic/libc/upstream-openbsd/android/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_gdtoa
LOCAL_SRC_FILES := \
	bionic/libc/upstream-openbsd/android/gdtoa_support.cpp \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/dmisc.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/dtoa.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/gdtoa.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/gethex.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/gmisc.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/hd_init.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/hdtoa.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/hexnan.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/ldtoa.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/misc.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/smisc.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/strtod.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/strtodg.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/strtof.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/strtord.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/sum.c \
	bionic/libc/upstream-openbsd/lib/libc/gdtoa/ulp.c
LOCAL_SRC_FILES_64 := bionic/libc/upstream-openbsd/lib/libc/gdtoa/strtorQ.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-Wno-sign-compare \
	-include openbsd-compat.h
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/private \
	-Ibionic/libc/upstream-openbsd/android/include \
	-Ibionic/libc/upstream-openbsd/lib/libc/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_fortify
LOCAL_SRC_FILES := bionic/libc/bionic/fortify.cpp
LOCAL_SRC_FILES_arm := \
	bionic/libc/arch-arm/generic/bionic/__memcpy_chk.S \
	bionic/libc/arch-arm/cortex-a15/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/cortex-a15/bionic/__strcpy_chk.S \
	bionic/libc/arch-arm/cortex-a7/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/cortex-a7/bionic/__strcpy_chk.S \
	bionic/libc/arch-arm/cortex-a9/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/cortex-a9/bionic/__strcpy_chk.S \
	bionic/libc/arch-arm/krait/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/krait/bionic/__strcpy_chk.S \
	bionic/libc/arch-arm/cortex-a53/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/cortex-a53/bionic/__strcpy_chk.S \
	bionic/libc/arch-arm/cortex-a55/bionic/__strcat_chk.S \
	bionic/libc/arch-arm/cortex-a55/bionic/__strcpy_chk.S
LOCAL_SRC_FILES_arm64 := bionic/libc/arch-arm64/generic/bionic/__memcpy_chk.S
LOCAL_CFLAGS := $(libc_common_flags) \
	-U_FORTIFY_SOURCE \
	-D__BIONIC_DECLARE_FORTIFY_HELPERS
LOCAL_CFLAGS_arm := \
	-DNO___MEMCPY_CHK \
	-DRENAME___STRCAT_CHK \
	-DRENAME___STRCPY_CHK
LOCAL_CFLAGS_arm64 := -DNO___MEMCPY_CHK
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_bionic
LOCAL_SRC_FILES := \
	bionic/libc/bionic/sysconf.cpp \
	bionic/libc/bionic/vdso.cpp \
	bionic/libc/bionic/setjmp_cookie.cpp \
	bionic/libc/bionic/android_set_abort_message.cpp \
	bionic/libc/bionic/strchr.cpp \
	bionic/libc/bionic/strchrnul.cpp \
	bionic/libc/bionic/strnlen.c \
	bionic/libc/bionic/strrchr.cpp
LOCAL_SRC_FILES_arm := \
	bionic/libc/arch-arm/generic/bionic/memcmp.S \
	bionic/libc/arch-arm/generic/bionic/memmove.S \
	bionic/libc/arch-arm/generic/bionic/memset.S \
	bionic/libc/arch-arm/generic/bionic/stpcpy.c \
	bionic/libc/arch-arm/generic/bionic/strcat.c \
	bionic/libc/arch-arm/generic/bionic/strcmp.S \
	bionic/libc/arch-arm/generic/bionic/strcpy.S \
	bionic/libc/arch-arm/generic/bionic/strlen.c \
	bionic/libc/arch-arm/bionic/__aeabi_read_tp.S \
	bionic/libc/arch-arm/bionic/__bionic_clone.S \
	bionic/libc/arch-arm/bionic/__restore.S \
	bionic/libc/arch-arm/bionic/_exit_with_stack_teardown.S \
	bionic/libc/arch-arm/bionic/atomics_arm.c \
	bionic/libc/arch-arm/bionic/bpabi.c \
	bionic/libc/arch-arm/bionic/libcrt_compat.c \
	bionic/libc/arch-arm/bionic/popcount_tab.c \
	bionic/libc/arch-arm/bionic/setjmp.S \
	bionic/libc/arch-arm/bionic/syscall.S \
	bionic/libc/arch-arm/bionic/vfork.S \
	bionic/libc/arch-arm/cortex-a15/bionic/memcpy.S \
	bionic/libc/arch-arm/cortex-a15/bionic/memmove.S \
	bionic/libc/arch-arm/cortex-a15/bionic/memset.S \
	bionic/libc/arch-arm/cortex-a15/bionic/stpcpy.S \
	bionic/libc/arch-arm/cortex-a15/bionic/strcat.S \
	bionic/libc/arch-arm/cortex-a15/bionic/strcmp.S \
	bionic/libc/arch-arm/cortex-a15/bionic/strcpy.S \
	bionic/libc/arch-arm/cortex-a15/bionic/strlen.S \
	bionic/libc/arch-arm/cortex-a7/bionic/memcpy.S \
	bionic/libc/arch-arm/cortex-a7/bionic/memset.S \
	bionic/libc/arch-arm/cortex-a9/bionic/memcpy.S \
	bionic/libc/arch-arm/cortex-a9/bionic/memset.S \
	bionic/libc/arch-arm/cortex-a9/bionic/stpcpy.S \
	bionic/libc/arch-arm/cortex-a9/bionic/strcat.S \
	bionic/libc/arch-arm/cortex-a9/bionic/strcpy.S \
	bionic/libc/arch-arm/cortex-a9/bionic/strlen.S \
	bionic/libc/arch-arm/krait/bionic/memcpy.S \
	bionic/libc/arch-arm/krait/bionic/memset.S \
	bionic/libc/arch-arm/cortex-a53/bionic/memcpy.S \
	bionic/libc/arch-arm/cortex-a55/bionic/memcpy.S \
	bionic/libc/arch-arm/kryo/bionic/memcpy.S
LOCAL_SRC_FILES_arm64 := \
	bionic/libc/arch-arm64/generic/bionic/memcpy.S \
	bionic/libc/arch-arm64/generic/bionic/memmove.S \
	bionic/libc/arch-arm64/generic/bionic/memset.S \
	bionic/libc/arch-arm64/generic/bionic/wmemmove.S \
	bionic/libc/arch-arm64/bionic/__bionic_clone.S \
	bionic/libc/arch-arm64/bionic/_exit_with_stack_teardown.S \
	bionic/libc/arch-arm64/bionic/setjmp.S \
	bionic/libc/arch-arm64/bionic/syscall.S \
	bionic/libc/arch-arm64/bionic/vfork.S
LOCAL_SRC_FILES_x86 := \
	bionic/libc/arch-x86/generic/string/memcmp.S \
	bionic/libc/arch-x86/generic/string/strcmp.S \
	bionic/libc/arch-x86/generic/string/strncmp.S \
	bionic/libc/arch-x86/generic/string/strcat.S \
	bionic/libc/arch-x86/generic/string/strlcat.c \
	bionic/libc/arch-x86/generic/string/strlcpy.c \
	bionic/libc/arch-x86/generic/string/strncat.c \
	bionic/libc/arch-x86/generic/string/wcscat.c \
	bionic/libc/arch-x86/generic/string/wcscpy.c \
	bionic/libc/arch-x86/generic/string/wmemcmp.c \
	bionic/libc/arch-x86/generic/string/wmemset.c \
	bionic/libc/arch-x86/atom/string/sse2-memchr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-memrchr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-strchr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-strnlen-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-strrchr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-wcschr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-wcsrchr-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-wcslen-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-wcscmp-atom.S \
	bionic/libc/arch-x86/silvermont/string/sse2-memmove-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-memset-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-stpcpy-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-stpncpy-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-strcpy-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-strlen-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse2-strncpy-slm.S \
	bionic/libc/arch-x86/bionic/__bionic_clone.S \
	bionic/libc/arch-x86/bionic/_exit_with_stack_teardown.S \
	bionic/libc/arch-x86/bionic/libcrt_compat.c \
	bionic/libc/arch-x86/bionic/__restore.S \
	bionic/libc/arch-x86/bionic/setjmp.S \
	bionic/libc/arch-x86/bionic/syscall.S \
	bionic/libc/arch-x86/bionic/vfork.S \
	bionic/libc/arch-x86/bionic/__x86.get_pc_thunk.S \
	bionic/libc/arch-x86/atom/string/ssse3-strcat-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strcmp-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strlcat-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strlcpy-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strncat-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strncmp-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-wcscat-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-wcscpy-atom.S \
	bionic/libc/arch-x86/silvermont/string/sse4-memcmp-slm.S \
	bionic/libc/arch-x86/silvermont/string/sse4-wmemcmp-slm.S \
	bionic/libc/arch-x86/atom/string/sse2-memset-atom.S \
	bionic/libc/arch-x86/atom/string/sse2-strlen-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-memcmp-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-memmove-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strcpy-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-strncpy-atom.S \
	bionic/libc/arch-x86/atom/string/ssse3-wmemcmp-atom.S \
	bionic/libc/arch-x86/kabylake/string/avx2-wmemset-kbl.S
LOCAL_SRC_FILES_x86_64 := \
	bionic/libc/arch-x86_64/string/sse2-memmove-slm.S \
	bionic/libc/arch-x86_64/string/sse2-memset-slm.S \
	bionic/libc/arch-x86_64/string/sse2-stpcpy-slm.S \
	bionic/libc/arch-x86_64/string/sse2-stpncpy-slm.S \
	bionic/libc/arch-x86_64/string/sse2-strcat-slm.S \
	bionic/libc/arch-x86_64/string/sse2-strcpy-slm.S \
	bionic/libc/arch-x86_64/string/sse2-strlen-slm.S \
	bionic/libc/arch-x86_64/string/sse2-strncat-slm.S \
	bionic/libc/arch-x86_64/string/sse2-strncpy-slm.S \
	bionic/libc/arch-x86_64/string/sse4-memcmp-slm.S \
	bionic/libc/arch-x86_64/string/ssse3-strcmp-slm.S \
	bionic/libc/arch-x86_64/string/ssse3-strncmp-slm.S \
	bionic/libc/arch-x86_64/string/avx2-wmemset-kbl.S \
	bionic/libc/arch-x86_64/bionic/__bionic_clone.S \
	bionic/libc/arch-x86_64/bionic/_exit_with_stack_teardown.S \
	bionic/libc/arch-x86_64/bionic/__restore_rt.S \
	bionic/libc/arch-x86_64/bionic/setjmp.S \
	bionic/libc/arch-x86_64/bionic/syscall.S \
	bionic/libc/arch-x86_64/bionic/vfork.S
LOCAL_SRC_FILES_EXCLUDE_arm64 := \
	bionic/libc/bionic/strchr.cpp \
	bionic/libc/bionic/strchrnul.cpp \
	bionic/libc/bionic/strnlen.c \
	bionic/libc/bionic/strrchr.cpp
LOCAL_SRC_FILES_EXCLUDE_x86 := \
	bionic/libc/bionic/strchr.cpp \
	bionic/libc/bionic/strnlen.c \
	bionic/libc/bionic/strrchr.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS_arm := -mno-restrict-it
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libstdc++/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libsystemproperties
LOCAL_SRC_FILES := \
	bionic/libc/system_properties/context_node.cpp \
	bionic/libc/system_properties/contexts_split.cpp \
	bionic/libc/system_properties/contexts_serialized.cpp \
	bionic/libc/system_properties/prop_area.cpp \
	bionic/libc/system_properties/prop_info.cpp \
	bionic/libc/system_properties/system_properties.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libstdc++/include \
	-Isystem/core/property_service/libpropertyinfoparser/include \
	-Ibionic/libc/system_properties/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libpropertyinfoparser.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_bionic_systrace
LOCAL_SRC_FILES := bionic/libc/bionic/bionic_systrace.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

$(builddir)/generated_android_ids.h: system/core/libcutils/include/cutils/android_filesystem_config.h
	@python2 bionic/libc/fs_config_generator.py aidarray system/core/libcutils/include/cutils/android_filesystem_config.h >> $@

LOCAL_MODULE := libc_bionic_ndk
LOCAL_SRC_FILES := \
	bionic/libc/bionic/NetdClientDispatch.cpp \
	bionic/libc/bionic/__bionic_get_shell_path.cpp \
	bionic/libc/bionic/__cmsg_nxthdr.cpp \
	bionic/libc/bionic/__errno.cpp \
	bionic/libc/bionic/__gnu_basename.cpp \
	bionic/libc/bionic/__libc_current_sigrtmax.cpp \
	bionic/libc/bionic/__libc_current_sigrtmin.cpp \
	bionic/libc/bionic/abort.cpp \
	bionic/libc/bionic/accept.cpp \
	bionic/libc/bionic/access.cpp \
	bionic/libc/bionic/arpa_inet.cpp \
	bionic/libc/bionic/assert.cpp \
	bionic/libc/bionic/atof.cpp \
	bionic/libc/bionic/bionic_allocator.cpp \
	bionic/libc/bionic/bionic_arc4random.cpp \
	bionic/libc/bionic/bionic_futex.cpp \
	bionic/libc/bionic/bionic_netlink.cpp \
	bionic/libc/bionic/bionic_time_conversions.cpp \
	bionic/libc/bionic/brk.cpp \
	bionic/libc/bionic/c16rtomb.cpp \
	bionic/libc/bionic/c32rtomb.cpp \
	bionic/libc/bionic/chmod.cpp \
	bionic/libc/bionic/chown.cpp \
	bionic/libc/bionic/clearenv.cpp \
	bionic/libc/bionic/clock.cpp \
	bionic/libc/bionic/clock_getcpuclockid.cpp \
	bionic/libc/bionic/clock_nanosleep.cpp \
	bionic/libc/bionic/clone.cpp \
	bionic/libc/bionic/ctype.cpp \
	bionic/libc/bionic/dirent.cpp \
	bionic/libc/bionic/dup.cpp \
	bionic/libc/bionic/environ.cpp \
	bionic/libc/bionic/error.cpp \
	bionic/libc/bionic/eventfd.cpp \
	bionic/libc/bionic/exec.cpp \
	bionic/libc/bionic/execinfo.cpp \
	bionic/libc/bionic/faccessat.cpp \
	bionic/libc/bionic/fchmod.cpp \
	bionic/libc/bionic/fchmodat.cpp \
	bionic/libc/bionic/fcntl.cpp \
	bionic/libc/bionic/fdsan.cpp \
	bionic/libc/bionic/fdtrack.cpp \
	bionic/libc/bionic/ffs.cpp \
	bionic/libc/bionic/fgetxattr.cpp \
	bionic/libc/bionic/flistxattr.cpp \
	bionic/libc/bionic/flockfile.cpp \
	bionic/libc/bionic/fpclassify.cpp \
	bionic/libc/bionic/fsetxattr.cpp \
	bionic/libc/bionic/ftruncate.cpp \
	bionic/libc/bionic/ftw.cpp \
	bionic/libc/bionic/futimens.cpp \
	bionic/libc/bionic/getcwd.cpp \
	bionic/libc/bionic/getdomainname.cpp \
	bionic/libc/bionic/getentropy.cpp \
	bionic/libc/bionic/gethostname.cpp \
	bionic/libc/bionic/getloadavg.cpp \
	bionic/libc/bionic/getpagesize.cpp \
	bionic/libc/bionic/getpgrp.cpp \
	bionic/libc/bionic/getpid.cpp \
	bionic/libc/bionic/getpriority.cpp \
	bionic/libc/bionic/gettid.cpp \
	bionic/libc/bionic/get_device_api_level.cpp \
	bionic/libc/bionic/grp_pwd.cpp \
	bionic/libc/bionic/grp_pwd_file.cpp \
	bionic/libc/bionic/heap_zero_init.cpp \
	bionic/libc/bionic/iconv.cpp \
	bionic/libc/bionic/icu_wrappers.cpp \
	bionic/libc/bionic/ifaddrs.cpp \
	bionic/libc/bionic/inotify_init.cpp \
	bionic/libc/bionic/ioctl.cpp \
	bionic/libc/bionic/killpg.cpp \
	bionic/libc/bionic/langinfo.cpp \
	bionic/libc/bionic/lchown.cpp \
	bionic/libc/bionic/lfs64_support.cpp \
	bionic/libc/bionic/libc_init_common.cpp \
	bionic/libc/bionic/libgen.cpp \
	bionic/libc/bionic/link.cpp \
	bionic/libc/bionic/locale.cpp \
	bionic/libc/bionic/lockf.cpp \
	bionic/libc/bionic/lstat.cpp \
	bionic/libc/bionic/mblen.cpp \
	bionic/libc/bionic/mbrtoc16.cpp \
	bionic/libc/bionic/mbrtoc32.cpp \
	bionic/libc/bionic/mempcpy.cpp \
	bionic/libc/bionic/mkdir.cpp \
	bionic/libc/bionic/mkfifo.cpp \
	bionic/libc/bionic/mknod.cpp \
	bionic/libc/bionic/mntent.cpp \
	bionic/libc/bionic/mremap.cpp \
	bionic/libc/bionic/net_if.cpp \
	bionic/libc/bionic/netdb.cpp \
	bionic/libc/bionic/netinet_in.cpp \
	bionic/libc/bionic/nl_types.cpp \
	bionic/libc/bionic/open.cpp \
	bionic/libc/bionic/pathconf.cpp \
	bionic/libc/bionic/pause.cpp \
	bionic/libc/bionic/pidfd.cpp \
	bionic/libc/bionic/pipe.cpp \
	bionic/libc/bionic/poll.cpp \
	bionic/libc/bionic/posix_fadvise.cpp \
	bionic/libc/bionic/posix_fallocate.cpp \
	bionic/libc/bionic/posix_madvise.cpp \
	bionic/libc/bionic/posix_timers.cpp \
	bionic/libc/bionic/preadv_pwritev.cpp \
	bionic/libc/bionic/ptrace.cpp \
	bionic/libc/bionic/pty.cpp \
	bionic/libc/bionic/raise.cpp \
	bionic/libc/bionic/rand.cpp \
	bionic/libc/bionic/readlink.cpp \
	bionic/libc/bionic/realpath.cpp \
	bionic/libc/bionic/reboot.cpp \
	bionic/libc/bionic/recv.cpp \
	bionic/libc/bionic/recvmsg.cpp \
	bionic/libc/bionic/rename.cpp \
	bionic/libc/bionic/rmdir.cpp \
	bionic/libc/bionic/scandir.cpp \
	bionic/libc/bionic/sched_getaffinity.cpp \
	bionic/libc/bionic/sched_getcpu.cpp \
	bionic/libc/bionic/semaphore.cpp \
	bionic/libc/bionic/send.cpp \
	bionic/libc/bionic/setegid.cpp \
	bionic/libc/bionic/seteuid.cpp \
	bionic/libc/bionic/setpgrp.cpp \
	bionic/libc/bionic/sigaction.cpp \
	bionic/libc/bionic/signal.cpp \
	bionic/libc/bionic/sigprocmask.cpp \
	bionic/libc/bionic/sleep.cpp \
	bionic/libc/bionic/socketpair.cpp \
	bionic/libc/bionic/spawn.cpp \
	bionic/libc/bionic/stat.cpp \
	bionic/libc/bionic/stdlib_l.cpp \
	bionic/libc/bionic/strerror.cpp \
	bionic/libc/bionic/string_l.cpp \
	bionic/libc/bionic/strings_l.cpp \
	bionic/libc/bionic/strsignal.cpp \
	bionic/libc/bionic/strtol.cpp \
	bionic/libc/bionic/strtold.cpp \
	bionic/libc/bionic/swab.cpp \
	bionic/libc/bionic/symlink.cpp \
	bionic/libc/bionic/sync_file_range.cpp \
	bionic/libc/bionic/sys_epoll.cpp \
	bionic/libc/bionic/sys_msg.cpp \
	bionic/libc/bionic/sys_sem.cpp \
	bionic/libc/bionic/sys_shm.cpp \
	bionic/libc/bionic/sys_signalfd.cpp \
	bionic/libc/bionic/sys_statfs.cpp \
	bionic/libc/bionic/sys_statvfs.cpp \
	bionic/libc/bionic/sys_time.cpp \
	bionic/libc/bionic/sysinfo.cpp \
	bionic/libc/bionic/syslog.cpp \
	bionic/libc/bionic/system.cpp \
	bionic/libc/bionic/system_property_api.cpp \
	bionic/libc/bionic/system_property_set.cpp \
	bionic/libc/bionic/tdestroy.cpp \
	bionic/libc/bionic/termios.cpp \
	bionic/libc/bionic/thread_private.cpp \
	bionic/libc/bionic/threads.cpp \
	bionic/libc/bionic/timespec_get.cpp \
	bionic/libc/bionic/tmpfile.cpp \
	bionic/libc/bionic/umount.cpp \
	bionic/libc/bionic/unlink.cpp \
	bionic/libc/bionic/usleep.cpp \
	bionic/libc/bionic/utmp.cpp \
	bionic/libc/bionic/wait.cpp \
	bionic/libc/bionic/wchar.cpp \
	bionic/libc/bionic/wchar_l.cpp \
	bionic/libc/bionic/wcstod.cpp \
	bionic/libc/bionic/wctype.cpp \
	bionic/libc/bionic/wcwidth.cpp \
	bionic/libc/bionic/wmempcpy.cpp \
	bionic/libc/bionic/icu_static.cpp
LOCAL_SRC_FILES_32 := bionic/libc/bionic/mmap.cpp
LOCAL_CFLAGS := $(libc_common_flags) \
	-DTREBLE_LINKER_NAMESPACES \
	-Wno-unused-function \
	-Wno-macro-redefined
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-I$(builddir) \
	-Ibionic/libc/stdio \
	-Ibionic/libstdc++/include \
	-Isystem/core/property_service/libpropertyinfoparser/include \
	-Ibionic/libc/system_properties/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libc_bionic_systrace.a \
	$(builddir)/libsystemproperties.a
LOCAL_EXTRA_DEPS := $(builddir)/generated_android_ids.h

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_pthread
LOCAL_SRC_FILES := \
	bionic/libc/bionic/bionic_elf_tls.cpp \
	bionic/libc/bionic/pthread_atfork.cpp \
	bionic/libc/bionic/pthread_attr.cpp \
	bionic/libc/bionic/pthread_barrier.cpp \
	bionic/libc/bionic/pthread_cond.cpp \
	bionic/libc/bionic/pthread_create.cpp \
	bionic/libc/bionic/pthread_detach.cpp \
	bionic/libc/bionic/pthread_equal.cpp \
	bionic/libc/bionic/pthread_exit.cpp \
	bionic/libc/bionic/pthread_getcpuclockid.cpp \
	bionic/libc/bionic/pthread_getschedparam.cpp \
	bionic/libc/bionic/pthread_gettid_np.cpp \
	bionic/libc/bionic/pthread_internal.cpp \
	bionic/libc/bionic/pthread_join.cpp \
	bionic/libc/bionic/pthread_key.cpp \
	bionic/libc/bionic/pthread_kill.cpp \
	bionic/libc/bionic/pthread_mutex.cpp \
	bionic/libc/bionic/pthread_once.cpp \
	bionic/libc/bionic/pthread_rwlock.cpp \
	bionic/libc/bionic/pthread_sigqueue.cpp \
	bionic/libc/bionic/pthread_self.cpp \
	bionic/libc/bionic/pthread_setname_np.cpp \
	bionic/libc/bionic/pthread_setschedparam.cpp \
	bionic/libc/bionic/pthread_spinlock.cpp \
	bionic/libc/bionic/sys_thread_properties.cpp \
	bionic/libc/bionic/__cxa_thread_atexit_impl.cpp \
	bionic/libc/bionic/android_unsafe_frame_pointer_chase.cpp \
	bionic/libc/bionic/atexit.cpp \
	bionic/libc/bionic/fork.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := \
	$(libc_include_dirs) \
	-Ibionic/libstdc++/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)

include static_library.mk

include cleanvars.mk

$(builddir)/syscalls-$(ARCH).S: bionic/libc/SYSCALLS.TXT
	@python3 bionic/libc/tools/gensyscalls.py $(ARCH) bionic/libc/SYSCALLS.TXT >> $@

LOCAL_MODULE := libc_syscalls
LOCAL_SRC_FILES := \
	bionic/libc/bionic/__set_errno.cpp \
	$(builddir)/syscalls-$(ARCH).S
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_LDFLAGS := $(libc_ldflags)
LOCAL_MODULE_NO_STL_STD := true
LOCAL_EXTRA_DEPS := $(builddir)/syscalls-$(ARCH).S

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_aeabi
LOCAL_SRC_FILES_arm := bionic/libc/arch-arm/bionic/__aeabi.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-fno-builtin
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libasync_safe
LOCAL_SRC_FILES := bionic/libc/async_safe/async_safe_log.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libc/async_safe/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libstdc++
LOCAL_SRC_FILES := \
	bionic/libc/bionic/__cxa_guard.cpp \
	bionic/libc/bionic/__cxa_pure_virtual.cpp \
	bionic/libc/bionic/new.cpp
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs) \
	-Ibionic/libstdc++/include
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_nopthread
LOCAL_SRC_FILES := $(libc_common_src_files)
LOCAL_SRC_FILES_32 := $(libc_common_src_files_32)
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libarm-optimized-routines-string.a \
	$(builddir)/libasync_safe.a \
	$(builddir)/libc_bionic.a \
	$(builddir)/libc_bionic_ndk.a \
	$(builddir)/libc_bootstrap.a \
	$(builddir)/libc_dns.a \
	$(builddir)/libc_fortify.a \
	$(builddir)/libc_freebsd.a \
	$(builddir)/libc_freebsd_large_stack.a \
	$(builddir)/libc_gdtoa.a \
	$(builddir)/libc_netbsd.a \
	$(builddir)/libc_openbsd.a \
	$(builddir)/libc_openbsd_large_stack.a \
	$(builddir)/libc_openbsd_ndk.a \
	$(builddir)/libc_syscalls.a \
	$(builddir)/libc_tzcode.a \
	$(builddir)/libstdc++.a
LOCAL_WHOLE_STATIC_LIBRARIES_arm := \
	$(builddir)/libc_aeabi.a
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_common
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libc_nopthread.a \
	$(builddir)/libc_pthread.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_static_dispatch
LOCAL_SRC_FILES_arm := bionic/libc/arch-arm/static_function_dispatch.S
LOCAL_SRC_FILES_arm64 := bionic/libc/arch-arm64/static_function_dispatch.S
LOCAL_SRC_FILES_x86 := bionic/libc/arch-x86/static_function_dispatch.S
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_dynamic_dispatch
LOCAL_SRC_FILES_arm := bionic/libc/arch-arm/dynamic_function_dispatch.cpp
LOCAL_SRC_FILES_arm64 := bionic/libc/arch-arm64/dynamic_function_dispatch.cpp
LOCAL_SRC_FILES_x86 := bionic/libc/arch-x86/dynamic_function_dispatch.cpp
LOCAL_CFLAGS := $(libc_common_flags) \
	-ffreestanding \
	-fno-stack-protector \
	-fno-jump-tables
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_common_static
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libc_common.a \
	$(builddir)/libc_static_dispatch.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_common_shared
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libc_common.a \
	$(builddir)/libc_dynamic_dispatch.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_unwind_static
LOCAL_SRC_FILES := bionic/libc/bionic/dl_iterate_phdr_static.cpp
LOCAL_SRC_FILES_arm := bionic/libc/arch-arm/bionic/exidx_static.c
LOCAL_CFLAGS := $(libc_common_flags) \
	-DLIBC_STATIC
LOCAL_ASFLAGS := $(libc_common_flags)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc_nomalloc
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libc_common.a \
	$(builddir)/libc_init_static.a \
	$(builddir)/libc_unwind_static.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc
LOCAL_SRC_FILES := \
	bionic/libc/bionic/gwp_asan_wrappers.cpp \
	bionic/libc/bionic/heap_tagging.cpp \
	bionic/libc/bionic/malloc_common.cpp \
	bionic/libc/bionic/malloc_limit.cpp
LOCAL_CFLAGS := $(libc_common_flags) \
	-DLIBC_STATIC
LOCAL_ASFLAGS := $(libc_common_flags) \
	-DPLATFORM_SDK_VERSION=$(AndroidApiLevel)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/gwp_asan.a \
	$(builddir)/libc_init_static.a \
	$(builddir)/libc_common_static.a \
	$(builddir)/libc_unwind_static.a \
	$(builddir)/libscudo.a
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

LOCAL_MODULE := libc
LOCAL_SRC_FILES := \
	bionic/libc/arch-common/bionic/crtbegin_so.c \
	bionic/libc/arch-common/bionic/crtbrand.S \
	bionic/libc/bionic/gwp_asan_wrappers.cpp \
	bionic/libc/bionic/heap_tagging.cpp \
	bionic/libc/bionic/icu.cpp \
	bionic/libc/bionic/malloc_common.cpp \
	bionic/libc/bionic/malloc_common_dynamic.cpp \
	bionic/libc/bionic/android_profiling_dynamic.cpp \
	bionic/libc/bionic/malloc_heapprofd.cpp \
	bionic/libc/bionic/malloc_limit.cpp \
	bionic/libc/bionic/ndk_cruft.cpp \
	bionic/libc/bionic/ndk_cruft_data.cpp \
	bionic/libc/bionic/NetdClient.cpp \
	bionic/libc/arch-common/bionic/crtend_so.S
LOCAL_SRC_FILES_arm := \
	bionic/libc/arch-arm/bionic/exidx_dynamic.c \
	bionic/libc/arch-arm/bionic/atexit_legacy.c
LOCAL_CFLAGS := $(libc_common_flags)
LOCAL_CFLAGS_arm := -DCRT_LEGACY_WORKAROUND
LOCAL_ASFLAGS := $(libc_common_flags) \
	-DPLATFORM_SDK_VERSION=$(AndroidApiLevel)
LOCAL_C_INCLUDES := $(libc_include_dirs)
LOCAL_C_ONLY_FLAGS := $(libc_conlyflags)
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/ld-android.so \
	$(builddir)/libdl.so
LOCAL_STATIC_LIBRARIES := $(builddir)/libdl_android.a
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/gwp_asan.a \
	$(builddir)/libc_init_dynamic.a \
	$(builddir)/libc_common_shared.a \
	$(builddir)/libscudo.a \
	$(LIBUNWIND_EXPORTED)
LOCAL_LDFLAGS := $(libc_ldflags) \
	-Wl,--version-script,$(builddir)/libc_shared/objs/bionic/libc/libc.map
LOCAL_LDFLAGS_arm := -Wl,--hash-style=both
LOCAL_LDFLAGS_x86 := -Wl,--hash-style=both
LOCAL_API_STUBS := bionic/libc/libc.map.txt
LOCAL_MODULE_NO_STL_STD := true
LOCAL_USE_CRT := false

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libstdc++
LOCAL_EXTRA_DEPS := $(builddir)/libstdc++.a $(builddir)/libc.so
LOCAL_SHARED_LIBRARIES := $(builddir)/libc.so
LOCAL_WHOLE_STATIC_LIBRARIES := \
	$(builddir)/libasync_safe.a \
	$(builddir)/libstdc++.a
LOCAL_LDFLAGS := $(libc_ldflags) \
	-Wl,--version-script,$(builddir)/libstdc++_shared/objs/bionic/libc/libstdc++.map
LOCAL_LDFLAGS_arm := -Wl,--hash-style=both
LOCAL_LDFLAGS_x86 := -Wl,--hash-style=both
LOCAL_API_STUBS := bionic/libc/libstdc++.map.txt

include shared_library.mk
