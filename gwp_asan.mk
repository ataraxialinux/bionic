include defines.mk
include cleanvars.mk

LOCAL_MODULE := gwp_asan
LOCAL_SRC_FILES := \
	external/gwp_asan/gwp_asan/common.cpp \
	external/gwp_asan/gwp_asan/guarded_pool_allocator.cpp \
	external/gwp_asan/gwp_asan/platform_specific/common_posix.cpp \
	external/gwp_asan/gwp_asan/platform_specific/guarded_pool_allocator_posix.cpp \
	external/gwp_asan/gwp_asan/platform_specific/mutex_posix.cpp \
	external/gwp_asan/gwp_asan/platform_specific/utilities_posix.cpp \
	external/gwp_asan/gwp_asan/stack_trace_compressor.cpp
LOCAL_CFLAGS := -fno-emulated-tls
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Iexternal/gwp_asan
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk
