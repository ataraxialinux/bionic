STEM_TYPE := shared_library

include base.mk

linked_module := $(intermediates)/$(notdir $(my_built_module_stem))

LOCAL_INTERMEDIATE_TARGETS := $(linked_module)

ifeq ($(LOCAL_NO_LIBCRT_BUILTINS),true)
my_target_libcrt_builtins :=
else
my_target_libcrt_builtins := $(LIBCRT_BUILTINS)
endif
ifeq ($(LOCAL_USE_CRT),false)
my_target_crtbegin_so_o :=
my_target_crtend_so_o :=
else
my_target_crtbegin_so_o := $(builddir)/crtbegin_so.o
my_target_crtend_so_o := $(builddir)/crtend_so.o
endif

$(linked_module): PRIVATE_TARGET_LIBCRT_BUILTINS := $(my_target_libcrt_builtins)
$(linked_module): PRIVATE_TARGET_CRTBEGIN_SO_O := $(my_target_crtbegin_so_o)
$(linked_module): PRIVATE_TARGET_CRTEND_SO_O := $(my_target_crtend_so_o)

include binary.mk

$(linked_module): $(LOCAL_EXTRA_DEPS) $(my_target_crtbegin_so_o) $(my_target_crtend_so_o) $(all_objects) $(stub_sources)
	$(call transform-o-to-shared-lib)
