include defines.mk

include cleanvars.mk

LOCAL_MODULE := libzstd
LOCAL_SRC_FILES := \
	external/zstd/lib/common/debug.c \
	external/zstd/lib/common/entropy_common.c \
	external/zstd/lib/common/error_private.c \
	external/zstd/lib/common/fse_decompress.c \
	external/zstd/lib/common/pool.c \
	external/zstd/lib/common/threading.c \
	external/zstd/lib/common/xxhash.c \
	external/zstd/lib/common/zstd_common.c \
	external/zstd/lib/compress/fse_compress.c \
	external/zstd/lib/compress/hist.c \
	external/zstd/lib/compress/huf_compress.c \
	external/zstd/lib/compress/zstd_compress.c \
	external/zstd/lib/compress/zstd_compress_literals.c \
	external/zstd/lib/compress/zstd_compress_sequences.c \
	external/zstd/lib/compress/zstd_compress_superblock.c \
	external/zstd/lib/compress/zstd_double_fast.c \
	external/zstd/lib/compress/zstd_fast.c \
	external/zstd/lib/compress/zstd_lazy.c \
	external/zstd/lib/compress/zstd_ldm.c \
	external/zstd/lib/compress/zstdmt_compress.c \
	external/zstd/lib/compress/zstd_opt.c \
	external/zstd/lib/decompress/huf_decompress.c \
	external/zstd/lib/decompress/zstd_ddict.c \
	external/zstd/lib/decompress/zstd_decompress_block.c \
	external/zstd/lib/decompress/zstd_decompress.c \
	external/zstd/lib/deprecated/zbuff_common.c \
	external/zstd/lib/deprecated/zbuff_compress.c \
	external/zstd/lib/deprecated/zbuff_decompress.c \
	external/zstd/lib/dictBuilder/cover.c \
	external/zstd/lib/dictBuilder/divsufsort.c \
	external/zstd/lib/dictBuilder/fastcover.c \
	external/zstd/lib/dictBuilder/zdict.c \
	external/zstd/lib/legacy/zstd_v01.c \
	external/zstd/lib/legacy/zstd_v02.c \
	external/zstd/lib/legacy/zstd_v03.c \
	external/zstd/lib/legacy/zstd_v04.c \
	external/zstd/lib/legacy/zstd_v05.c \
	external/zstd/lib/legacy/zstd_v06.c \
	external/zstd/lib/legacy/zstd_v07.c
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-but-set-variable
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Iexternal/zstd/lib/common \
	-Iexternal/zstd/lib

include static_library.mk
