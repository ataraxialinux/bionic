STEM_TYPE := executable

include base.mk

linked_module := $(intermediates)/$(notdir $(my_built_module_stem))

LOCAL_INTERMEDIATE_TARGETS := $(linked_module)

ifeq ($(LOCAL_NO_LIBCRT_BUILTINS),true)
my_target_libcrt_builtins :=
else
my_target_libcrt_builtins := $(LIBCRT_BUILTINS)
endif
ifeq ($(LOCAL_USE_CRT),false)
my_target_crtbegin_dynamic_o :=
my_target_crtbegin_static_o :=
my_target_crtend_o :=
else
my_target_crtbegin_dynamic_o := $(builddir)/crtbegin_dynamic.o
my_target_crtbegin_static_o := $(builddir)/crtbegin_static.o
my_target_crtend_o := $(builddir)/crtend_android.o
endif

$(linked_module): PRIVATE_TARGET_LIBCRT_BUILTINS := $(my_target_libcrt_builtins)
$(linked_module): PRIVATE_TARGET_CRTBEGIN_DYNAMIC_O := $(my_target_crtbegin_dynamic_o)
$(linked_module): PRIVATE_TARGET_CRTBEGIN_STATIC_O := $(my_target_crtbegin_static_o)
$(linked_module): PRIVATE_TARGET_CRTEND_O := $(my_target_crtend_o)

include binary.mk

ifeq ($(LOCAL_FORCE_STATIC_EXECUTABLE),true)
$(linked_module): $(my_target_crtbegin_static_o) $(all_objects) $(stub_sources) $(my_target_crtend_o) $(my_target_libcrt_builtins)
	$(transform-o-to-static-executable)
else
$(linked_module): $(my_target_crtbegin_dynamic_o) $(all_objects) $(stub_sources) $(my_target_crtend_o) $(my_target_libcrt_builtins)
	$(transform-o-to-executable)
endif

