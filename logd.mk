include defines.mk

event_flag = \
	-DAUDITD_LOG_TAG=1003 \
	-DCHATTY_LOG_TAG=1004 \
	-DTAG_DEF_LOG_TAG=1005 \
	-DLIBLOG_LOG_TAG=1006

logd_cflags = \
	-Wextra \
	-Wthread-safety \
	$(event_flag)

include cleanvars.mk

LOCAL_MODULE := liblogd
LOCAL_SRC_FILES := \
	system/logging/logd/ChattyLogBuffer.cpp \
	system/logging/logd/CompressionEngine.cpp \
	system/logging/logd/LogBufferElement.cpp \
	system/logging/logd/LogReaderList.cpp \
	system/logging/logd/LogReaderThread.cpp \
	system/logging/logd/LogSize.cpp \
	system/logging/logd/LogStatistics.cpp \
	system/logging/logd/LogTags.cpp \
	system/logging/logd/LogdLock.cpp \
	system/logging/logd/PruneList.cpp \
	system/logging/logd/SerializedFlushToState.cpp \
	system/logging/logd/SerializedLogBuffer.cpp \
	system/logging/logd/SerializedLogChunk.cpp \
	system/logging/logd/SimpleLogBuffer.cpp
LOCAL_CFLAGS := $(logd_cflags)
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/zlib \
	-Iexternal/zstd/lib \
	-Isystem/libbase/include \
	-Isystem/core/libcutils/include \
	-Isystem/logging/logd

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := logd
LOCAL_SRC_FILES := \
	system/logging/logd/main.cpp \
	system/logging/logd/LogPermissions.cpp \
	system/logging/logd/CommandListener.cpp \
	system/logging/logd/LogListener.cpp \
	system/logging/logd/LogReader.cpp \
	system/logging/logd/LogAudit.cpp \
	system/logging/logd/LogKlog.cpp \
	system/logging/logd/libaudit.cpp \
	system/logging/logd/PkgIds.cpp
LOCAL_CFLAGS := $(logd_cflags) \
	-Wno-macro-redefined
LOCAL_C_INCLUDES := \
	-Iexternal/libcap/libcap/include \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/zlib \
	-Iexternal/zstd/lib \
	-Isystem/libbase/include \
	-Isystem/core/libcutils/include \
	-Isystem/core/libprocessgroup/include \
	-Isystem/core/libsysutils/include \
	-Isystem/core/libpackagelistparser/include \
	-Isystem/logging/logd
LOCAL_STATIC_LIBRARIES := \
	$(builddir)/liblog.a \
	$(builddir)/liblogd.a \
	$(builddir)/libzstd.a
LOCAL_EXTRA_DEPS := \
	$(builddir)/liblog.a \
	$(builddir)/liblogd.a \
	$(builddir)/libzstd.a \
	$(builddir)/libbase.so \
	$(builddir)/libz.so \
	$(builddir)/libsysutils.so \
	$(builddir)/libcutils.so \
	$(builddir)/libpackagelistparser.so \
	$(builddir)/libprocessgroup.so \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/libz.so \
	$(builddir)/libsysutils.so \
	$(builddir)/libcutils.so \
	$(builddir)/libpackagelistparser.so \
	$(builddir)/libprocessgroup.so \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so

include executable.mk

include cleanvars.mk

LOCAL_MODULE := liblogwrapper
LOCAL_SRC_FILES := \
	system/logging/logwrapper/logwrap.cpp
LOCAL_CFLAGS := \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/logging/logwrapper/include \
	-Isystem/libbase/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := liblogwrapper
LOCAL_EXTRA_DEPS := \
	$(builddir)/liblogwrapper.a \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/liblogwrapper.a

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := logwrapper
LOCAL_SRC_FILES := \
	system/logging/logwrapper/logwrap.cpp \
	system/logging/logwrapper/logwrapper.cpp
LOCAL_CFLAGS := \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/logging/logwrapper/include \
	-Isystem/libbase/include
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so

include executable.mk

include cleanvars.mk

LOCAL_MODULE := logcat
LOCAL_SRC_FILES := \
	system/logging/logcat/logcat.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-Werror \
	-DANDROID_BASE_UNIQUE_FD_DISABLE_IMPLICIT_CONVERSION=1
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/core/libprocessgroup/include
LOCAL_EXTRA_DEPS := \
	$(builddir)/liblog.a \
	$(builddir)/libbase.so \
	$(builddir)/libprocessgroup.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_STATIC_LIBRARIES := $(builddir)/liblog.a
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/libprocessgroup.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so

include executable.mk
