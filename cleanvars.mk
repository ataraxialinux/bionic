LOCAL_MODULE :=
LOCAL_BUILT_MODULE :=
LOCAL_SRC_FILES :=
LOCAL_SRC_FILES_$(ARCH) :=
LOCAL_SRC_FILES_32 :=
LOCAL_SRC_FILES_64 :=
LOCAL_SRC_FILES_EXCLUDE :=
LOCAL_SRC_FILES_EXCLUDE_$(ARCH) :=
LOCAL_SRC_FILES_EXCLUDE_32 :=
LOCAL_SRC_FILES_EXCLUDE_64 :=
LOCAL_SHARED_LIBRARIES :=
LOCAL_SHARED_LIBRARIES_$(ARCH) :=
LOCAL_SHARED_LIBRARIES_32 :=
LOCAL_SHARED_LIBRARIES_64 :=
LOCAL_STATIC_LIBRARIES :=
LOCAL_STATIC_LIBRARIES_$(ARCH) :=
LOCAL_STATIC_LIBRARIES_32 :=
LOCAL_STATIC_LIBRARIES_64 :=
LOCAL_WHOLE_STATIC_LIBRARIES :=
LOCAL_WHOLE_STATIC_LIBRARIES_$(ARCH) :=
LOCAL_WHOLE_STATIC_LIBRARIES_32 :=
LOCAL_WHOLE_STATIC_LIBRARIES_64 :=
LOCAL_ASFLAGS_$(ARCH) :=
LOCAL_CFLAGS_$(ARCH) :=
LOCAL_CPPFLAGS_$(ARCH) :=
LOCAL_C_INCLUDES_$(ARCH) :=
LOCAL_C_ONLY_FLAGS_$(ARCH) :=
LOCAL_ASFLAGS :=
LOCAL_CFLAGS :=
LOCAL_CPPFLAGS :=
LOCAL_C_INCLUDES :=
LOCAL_C_ONLY_FLAGS :=
LOCAL_ASFLAGS_32 :=
LOCAL_ASFLAGS_64 :=
LOCAL_CFLAGS_32 :=
LOCAL_CFLAGS_64 :=
LOCAL_CPPFLAGS_32 :=
LOCAL_CPPFLAGS_64 :=
LOCAL_C_INCLUDES_32 :=
LOCAL_C_INCLUDES_64 :=
LOCAL_C_ONLY_FLAGS_32 :=
LOCAL_C_ONLY_FLAGS_64 :=
LOCAL_LDFLAGS :=
LOCAL_LDFLAGS_$(ARCH) :=
LOCAL_LDFLAGS_32 :=
LOCAL_LDFLAGS_64 :=
LOCAL_LDLIBS :=
LOCAL_MODULE_IS_LIBRARY :=
LOCAL_MODULE_NO_STL_STD :=
LOCAL_C_STL_VERSION :=
LOCAL_CXX_STL_VERSION :=
LOCAL_USE_CRT := true
LOCAL_EXTRA_DEPS :=
LOCAL_API_STUBS := 
LOCAL_NO_LIBCRT_BUILTINS := false
LOCAL_FORCE_STATIC_EXECUTABLE := false
