ifeq ($(ARCH),)
$(error Specify architecture: arm, arm64, x86, x86_64)
endif

ifeq ($(LIBUNWIND),)
$(error Specify LIBUNWIND variable, must be path to libunwind.a)
endif

ifeq ($(LIBUNWIND_EXPORTED),)
$(error Specify LIBUNWIND_EXPORTED variable, must be path to libunwind-exported.a)
endif

AndroidBranch = master
AndroidApiLevel = 33

CC = clang
CXX = clang++
LD = ld.lld
AR = llvm-ar

HOSTCC = $(CC)
HOSTCXX = $(CXX)

asmTarget =
clangTarget =
uapiArch =
platformInitialCflags =
platformCflags =

armThumbCflags = -mthumb -Os
armCflags = -fomit-frame-pointer
armToolchainCflags = -msoft-float
armArchVariantCflags ?= -march=armv7-a -mfloat-abi=softfp -mfpu=neon
armLdflags = -Wl,--hash-style=gnu -Wl,-m,armelf
armLldflags =

arm64Cflags = -Werror=implicit-function-declaration
arm64ArchVariantCflags ?= -march=armv8-a
arm64Ldflags = -Wl,--hash-style=gnu -Wl,-z,separate-code
arm64Lldflags = -Wl,-z,max-page-size=4096

x86Cflags = -msse3 -mstackrealign
x86ArchVariantCflags ?= -march=prescott
x86Ldflags = -Wl,--hash-style=gnu

x86_64DefaultArchVariantFeatures = \
	-mssse3 \
	-msse4 \
	-msse4.1 \
	-msse4.2 \
	-mpopcnt
x86_64Cflags = $(x86_64DefaultArchVariantFeatures) -Werror=implicit-function-declaration
x86_64ArchVariantCflags ?= -march=x86-64
x86_64Ldflags = -Wl,--hash-style=gnu

ifeq ($(ARCH),arm)
asmTarget = arm-linux-androideabi
clangTarget = armv7a-linux-androideabi
uapiArch = arm
platformInitialCflags = $(armThumbCflags) $(armCflags)
platformCflags = $(armToolchainCflags) $(armArchVariantCflags) $(armCpuVariantCflags)
platformLdflags = $(armLdflags) $(armLldflags)
my_32_64_bit_suffix = 32
dynamic_linker = /system/bin/linker
endif

ifeq ($(ARCH),arm64)
asmTarget = aarch64-linux-android
clangTarget = aarch64-linux-android
uapiArch = arm64
platformInitialCflags = $(arm64Cflags)
platformCflags = $(arm64ArchVariantCflags) $(arm64CpuVariantCflags)
platformLdflags = $(arm64Ldflags) $(arm64Lldflags)
my_32_64_bit_suffix = 64
dynamic_linker = /system/bin/linker64
endif

ifeq ($(ARCH),x86)
asmTarget = i686-linux-android
clangTarget = i686-linux-android
uapiArch = x86
platformInitialCflags = $(x86Cflags)
platformCflags = $(x86ArchVariantCflags)
platformLdflags = $(x86Ldflags)
my_32_64_bit_suffix = 32
dynamic_linker = /system/bin/linker
endif

ifeq ($(ARCH),x86_64)
asmTarget = x86_64-linux-android
clangTarget = x86_64-linux-android
uapiArch = x86
platformInitialCflags = $(x86_64Cflags)
platformCflags = $(x86_64ArchVariantCflags)
platformLdflags = $(x86_64Ldflags)
my_32_64_bit_suffix = 64
dynamic_linker = /system/bin/linker64
endif

commonGlobalCflags = \
	-DANDROID \
	-fmessage-length=0 \
	-W \
	-Wall \
	-Wno-unused \
	-Winit-self \
	-Wpointer-arith \
	-Wunreachable-code-loop-increment \
	-no-canonical-prefixes \
	-DNDEBUG \
	-UDEBUG \
	-fno-exceptions \
	-Wno-multichar \
	-O2 \
	-g \
	-fdebug-info-for-profiling \
	-fno-strict-aliasing \
	-Werror=date-time \
	-Werror=pragma-pack \
	-Werror=pragma-pack-suspicious-include \
	-Werror=string-plus-int \
	-Werror=unreachable-code-loop-increment \
	-D__compiler_offsetof=__builtin_offsetof \
	-faddrsig \
	-fcommon \
	-Werror=int-conversion \
	-fexperimental-new-pass-manager \
	-Wno-reserved-id-macro \
	-Wno-unused-command-line-argument \
	-fcolor-diagnostics \
	-Wno-sign-compare \
	-Wno-defaulted-function-deleted \
	-Wno-inconsistent-missing-override \
	-Wno-c99-designator \
	-Wno-gnu-folding-constant \
	-Wunguarded-availability \
	-D__ANDROID_UNAVAILABLE_SYMBOLS_ARE_WEAK__ \
	-fdebug-prefix-map=/proc/self/cwd= \
	-ftrivial-auto-var-init=zero \
	-enable-trivial-auto-var-init-zero-knowing-it-will-be-removed-from-clang 

commonGlobalCppflags = \
	-Wsign-promo \
	-D_LIBCPP_ENABLE_THREAD_SAFETY_ANNOTATIONS \
	-Wno-gnu-include-next

deviceGlobalCflags = \
	-ffunction-sections \
	-fdata-sections \
	-fno-short-enums \
	-funwind-tables \
	-fstack-protector-strong \
	-Wa,--noexecstack \
	-D_FORTIFY_SOURCE=2 \
	-Wstrict-aliasing=2 \
	-Werror=return-type \
	-Werror=non-virtual-dtor \
	-Werror=address \
	-Werror=sequence-point \
	-Werror=format-security \
	-nostdlibinc

deviceGlobalCppflags = \
	-fvisibility-inlines-hidden

extraExternalCflags = \
	-Wno-enum-compare \
	-Wno-enum-compare-switch \
	-Wno-null-pointer-arithmetic \
	-Wno-null-dereference \
	-Wno-pointer-compare \
	-Wno-xor-used-as-pow \
	-Wno-final-dtor-non-final-class \
	-Wno-psabi \

noOverrideGlobalCflags = \
	-Werror=bool-operation \
	-Werror=implicit-int-float-conversion \
	-Werror=int-in-bool-context \
	-Werror=int-to-pointer-cast \
	-Werror=pointer-to-int-cast \
	-Werror=string-compare \
	-Werror=xor-used-as-pow \
	-Wno-void-pointer-to-enum-cast \
	-Wno-void-pointer-to-int-cast \
	-Wno-pointer-to-int-cast \
	-Werror=fortify-source \
	-Werror=address-of-temporary \
	-Werror=return-type \
	-Wno-tautological-constant-compare \
	-Wno-tautological-type-limit-compare \
	-Wno-reorder-init-list \
	-Wno-implicit-int-float-conversion \
	-Wno-int-in-bool-context \
	-Wno-sizeof-array-div \
	-Wno-tautological-overlap-compare \
	-Wno-deprecated-copy \
	-Wno-range-loop-construct \
	-Wno-misleading-indentation \
	-Wno-zero-as-null-pointer-constant \
	-Wno-deprecated-anon-enum-enum-conversion \
	-Wno-deprecated-enum-enum-conversion \
	-Wno-string-compare \
	-Wno-enum-enum-conversion \
	-Wno-enum-float-conversion \
	-Wno-pessimizing-move \
	-Wno-non-c-typedef-for-linkage \
	-Wno-string-concatenation

includeCflags = \
	-Isystem/core/include \
	-Isystem/logging/liblog/include \
	-Isystem/media/audio/include \
	-Ihardware/libhardware/include \
	-Ihardware/libhardware_legacy/include \
	-Ihardware/ril/include \
	-Iframeworks/native/include \
	-Iframeworks/native/opengl/include \
	-Iframeworks/av/include

deviceGlobalLdflags = \
	-Wl,-z,noexecstack \
	-Wl,-z,relro \
	-Wl,-z,now \
	-Wl,--build-id=md5 \
	-Wl,--warn-shared-textrel \
	-Wl,--fatal-warnings \
	-Wl,--no-undefined-version \
	-Wl,--exclude-libs,libgcc.a \
	-Wl,--exclude-libs,libgcc_stripped.a \
	-Wl,--exclude-libs,libunwind_llvm.a \
	-Wl,--exclude-libs,libunwind.a \
	-fuse-ld=lld

kernelHeaders = \
	-Ibionic/libc/kernel/uapi \
	-Ibionic/libc/kernel/android/scsi \
	-Ibionic/libc/kernel/android/uapi \
	-Ibionic/libc/kernel/uapi/asm-$(uapiArch)
bionicHeaders = -Ibionic/libc/include $(kernelHeaders)
bionicPlatformHeaders = -Ibionic/libc/platform $(bionicHeaders)

LIBCRT_BUILTINS ?= $(shell $(CC) -target $(clangTarget) -print-libgcc-file-name)
