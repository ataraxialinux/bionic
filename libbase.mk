include defines.mk

include cleanvars.mk

LOCAL_MODULE := libbase
LOCAL_SRC_FILES := \
	system/libbase/abi_compatibility.cpp \
	system/libbase/chrono_utils.cpp \
	system/libbase/cmsg.cpp \
	system/libbase/file.cpp \
	system/libbase/hex.cpp \
	system/libbase/logging.cpp \
	system/libbase/mapped_file.cpp \
	system/libbase/parsebool.cpp \
	system/libbase/parsenetaddress.cpp \
	system/libbase/process.cpp \
	system/libbase/properties.cpp \
	system/libbase/stringprintf.cpp \
	system/libbase/strings.cpp \
	system/libbase/threads.cpp \
	system/libbase/test_utils.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wextra \
	-D_FILE_OFFSET_BITS=64
LOCAL_CPPFLAGS := -Wexit-time-destructors
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libbase
LOCAL_EXTRA_DEPS := \
	$(builddir)/libbase.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/liblog.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/liblog.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libbase.a

include shared_library.mk
