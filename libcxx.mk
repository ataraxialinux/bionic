include defines.mk
include funcs.mk

builddir := $(call get-build-dir)

include cleanvars.mk

LOCAL_MODULE := libc++
LOCAL_SRC_FILES := \
	external/libcxx/src/algorithm.cpp \
	external/libcxx/src/any.cpp \
	external/libcxx/src/bind.cpp \
	external/libcxx/src/charconv.cpp \
	external/libcxx/src/chrono.cpp \
	external/libcxx/src/condition_variable.cpp \
	external/libcxx/src/debug.cpp \
	external/libcxx/src/exception.cpp \
	external/libcxx/src/future.cpp \
	external/libcxx/src/hash.cpp \
	external/libcxx/src/ios.cpp \
	external/libcxx/src/iostream.cpp \
	external/libcxx/src/locale.cpp \
	external/libcxx/src/memory.cpp \
	external/libcxx/src/mutex.cpp \
	external/libcxx/src/new.cpp \
	external/libcxx/src/optional.cpp \
	external/libcxx/src/random.cpp \
	external/libcxx/src/regex.cpp \
	external/libcxx/src/shared_mutex.cpp \
	external/libcxx/src/stdexcept.cpp \
	external/libcxx/src/string.cpp \
	external/libcxx/src/strstream.cpp \
	external/libcxx/src/system_error.cpp \
	external/libcxx/src/thread.cpp \
	external/libcxx/src/typeinfo.cpp \
	external/libcxx/src/utility.cpp \
	external/libcxx/src/valarray.cpp \
	external/libcxx/src/variant.cpp \
	external/libcxx/src/vector.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter
LOCAL_CPPFLAGS := \
	-fexceptions \
	-DLIBCXX_BUILDING_LIBCXXABI \
	-D_LIBCPP_BUILDING_LIBRARY
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/libcxxabi/include
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++14
LOCAL_RTTI_FLAG := -frtti
LOCAL_EXTRA_DEPS := $(builddir)/libc++abi.a
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libc++abi.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc++
LOCAL_EXTRA_DEPS := \
	$(builddir)/libc++.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libc++.a
LOCAL_MODULE_NO_STL_STD := true

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libc++experimental
LOCAL_SRC_FILES := external/libcxx/src/experimental/memory_resource.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter
LOCAL_CPPFLAGS := \
	-fexceptions \
	-DLIBCXX_BUILDING_LIBCXXABI \
	-D_LIBCPP_BUILDING_LIBRARY
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/libcxxabi/include
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++14
LOCAL_RTTI_FLAG := -frtti

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc++fs
LOCAL_SRC_FILES := \
	external/libcxx/src/filesystem/directory_iterator.cpp \
	external/libcxx/src/filesystem/operations.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter
LOCAL_CFLAGS_32 := -D_FILE_OFFSET_BITS=64
LOCAL_CPPFLAGS := \
	-fexceptions \
	-DLIBCXX_BUILDING_LIBCXXABI \
	-D_LIBCPP_BUILDING_LIBRARY
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/libcxxabi/include
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++14
LOCAL_RTTI_FLAG := -frtti

include static_library.mk
