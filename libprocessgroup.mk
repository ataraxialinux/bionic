include defines.mk

include cleanvars.mk

LOCAL_MODULE := libcgrouprc_format
LOCAL_SRC_FILES := system/core/libprocessgroup/cgrouprc_format/cgroup_controller.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/core/libprocessgroup/cgrouprc_format/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libcgrouprc
LOCAL_SRC_FILES := \
	system/core/libprocessgroup/cgrouprc/cgroup_controller.cpp \
	system/core/libprocessgroup/cgrouprc/cgroup_file.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/core/libprocessgroup/include \
	-Isystem/core/libprocessgroup/cgrouprc_format/include \
	-Isystem/core/libprocessgroup/cgrouprc/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libcgrouprc
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcgrouprc.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/libbase.so
LOCAL_STATIC_LIBRARIES := $(builddir)/libcgrouprc_format.a
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/libbase.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libcgrouprc.a
LOCAL_LDFLAGS := \
	-Wl,--version-script,$(builddir)/libcgrouprc_shared/objs/system/core/libprocessgroup/cgrouprc/libcgrouprc.map
LOCAL_API_STUBS := system/core/libprocessgroup/cgrouprc/libcgrouprc.map.txt

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libprocessgroup
LOCAL_SRC_FILES := \
	system/core/libprocessgroup/cgroup_map.cpp \
	system/core/libprocessgroup/processgroup.cpp \
	system/core/libprocessgroup/sched_policy.cpp \
	system/core/libprocessgroup/task_profiles.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wexit-time-destructors
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Iexternal/jsoncpp/include \
	-Isystem/libbase/include \
	-Isystem/core/libprocessgroup/cgrouprc/include \
	-Isystem/core/libprocessgroup \
	-Isystem/core/libprocessgroup/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libprocessgroup
LOCAL_EXTRA_DEPS := \
	$(builddir)/libprocessgroup.a \
	$(builddir)/libjsoncpp.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/libbase.so \
	$(builddir)/libcgrouprc.so
LOCAL_STATIC_LIBRARIES := $(builddir)/libjsoncpp.a
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/libbase.so \
	$(builddir)/libcgrouprc.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libprocessgroup.a

include shared_library.mk
