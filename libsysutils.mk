include defines.mk

include cleanvars.mk

LOCAL_MODULE := libsysutils
LOCAL_SRC_FILES := \
	system/core/libsysutils/src/SocketListener.cpp \
	system/core/libsysutils/src/FrameworkListener.cpp \
	system/core/libsysutils/src/NetlinkListener.cpp \
	system/core/libsysutils/src/NetlinkEvent.cpp \
	system/core/libsysutils/src/FrameworkCommand.cpp \
	system/core/libsysutils/src/SocketClient.cpp \
	system/core/libsysutils/src/ServiceManager.cpp
LOCAL_CFLAGS := \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/core/libsysutils/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libsysutils
LOCAL_EXTRA_DEPS := \
	$(builddir)/libsysutils.a \
	$(builddir)/libbase.so \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/libcutils.so \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libsysutils.a

include shared_library.mk
