include defines.mk

srcs_opt = \
	external/zlib/adler32_simd.c \
	external/zlib/crc32_simd.c

cflags_arm = \
	-DARMV8_OS_LINUX \
	-O3 \
	-DCPU_NO_SIMD

cflags_arm_neon = \
	-UCPU_NO_SIMD \
	-DADLER32_SIMD_NEON \
	-DCRC32_ARMV8_CRC32

cflags_arm64 = $(cflags_arm) $(cflags_arm_neon)

cflags_x86 = \
	-DX86_NOT_WINDOWS \
	-DCPU_NO_SIMD

cflags_android_x86 = \
	-UCPU_NO_SIMD \
	-DADLER32_SIMD_SSSE3 \
	-mpclmul \
	-DCRC32_SIMD_SSE42_PCLMUL

srcs_x86 = \
	external/zlib/crc_folding.c \
	external/zlib/fill_window_sse.c \
	$(srcs_opt)

cflags_64 = -DINFLATE_CHUNK_READ_64LE

libz_srcs = \
	external/zlib/adler32.c \
	external/zlib/compress.c \
	external/zlib/cpu_features.c \
	external/zlib/crc32.c \
	external/zlib/deflate.c \
	external/zlib/gzclose.c \
	external/zlib/gzlib.c \
	external/zlib/gzread.c \
	external/zlib/gzwrite.c \
	external/zlib/infback.c \
	external/zlib/inffast.c \
	external/zlib/inflate.c \
	external/zlib/inftrees.c \
	external/zlib/trees.c \
	external/zlib/uncompr.c \
	external/zlib/zutil.c

include cleanvars.mk

LOCAL_MODULE := libz
LOCAL_SRC_FILES := $(libz_srcs)
LOCAL_SRC_FILES_arm := $(srcs_opt)
LOCAL_SRC_FILES_arm64 := $(srcs_opt)
LOCAL_SRC_FILES_x86 := $(srcs_x86)
LOCAL_SRC_FILES_x86_64 := $(srcs_x86)
LOCAL_CFLAGS := \
	-DHAVE_HIDDEN \
	-DZLIB_CONST \
	-O3 \
	-Wall \
	-Werror \
	-Wno-unused \
	-Wno-unused-parameter
LOCAL_CFLAGS_arm := $(cflags_arm) $(cflags_arm_neon)
LOCAL_CFLAGS_arm64 := $(cflags_arm64) $(cflags_64)
LOCAL_CFLAGS_x86 := $(cflags_x86) $(cflags_android_x86)
LOCAL_CFLAGS_x86_64 := $(cflags_x86) $(cflags_64) $(cflags_android_x86)
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Iexternal/zlib
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libz
LOCAL_EXTRA_DEPS := \
	$(builddir)/libz.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libz.a
LOCAL_LDFLAGS_arm := -Wl,--hash-style=both

include shared_library.mk
