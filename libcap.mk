include defines.mk
include funcs.mk

builddir := $(call get-build-dir)

include cleanvars.mk

LOCAL_MODULE := libcap
LOCAL_SRC_FILES := \
	external/libcap/libcap/cap_alloc.c \
	external/libcap/libcap/cap_extint.c \
	external/libcap/libcap/cap_file.c \
	external/libcap/libcap/cap_flag.c \
	external/libcap/libcap/cap_proc.c \
	external/libcap/libcap/cap_text.c
LOCAL_CFLAGS := \
	-Wno-pointer-arith \
	-Wno-tautological-compare \
	-Wno-unused-parameter \
	-Wno-unused-result \
	-Wno-unused-variable
LOCAL_C_INCLUDES := \
	-Iexternal/libcap/libcap/include \
	$(bionicHeaders) \
	-I$(builddir)
LOCAL_EXTRA_DEPS := $(builddir)/cap_names.h

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libcap
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcap.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libcap.a

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := getcap
LOCAL_SRC_FILES := \
	external/libcap/progs/getcap.c
LOCAL_CFLAGS := \
	-Wno-pointer-arith \
	-Wno-tautological-compare \
	-Wno-unused-parameter \
	-Wno-unused-result \
	-Wno-unused-variable
LOCAL_C_INCLUDES := \
	-Iexternal/libcap/libcap/include \
	$(bionicHeaders)
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so

include executable.mk

include cleanvars.mk

LOCAL_MODULE := setcap
LOCAL_SRC_FILES := \
	external/libcap/progs/setcap.c
LOCAL_CFLAGS := \
	-Wno-pointer-arith \
	-Wno-tautological-compare \
	-Wno-unused-parameter \
	-Wno-unused-result \
	-Wno-unused-variable
LOCAL_C_INCLUDES := \
	-Iexternal/libcap/libcap/include \
	$(bionicHeaders)
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libcap.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so

include executable.mk

$(builddir)/cap_names.list.h: external/libcap/libcap/include/uapi/linux/capability.h
	@mkdir -p $(dir $@)
	@awk -f external/libcap/generate_cap_names_list.awk $< > $@

$(builddir)/_makenames: external/libcap/libcap/_makenames.c $(builddir)/cap_names.list.h
	@$(HOSTCC) -I$(builddir) external/libcap/libcap/_makenames.c -o $@

$(builddir)/cap_names.h: $(builddir)/_makenames
	@$(builddir)/_makenames > $@
