include defines.mk

include cleanvars.mk

LOCAL_MODULE := libc++demangle
LOCAL_SRC_FILES := \
	external/libcxxabi/src/cxa_demangle.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror
LOCAL_CPPFLAGS := \
	-fexceptions \
	-Wextra \
	-Wno-unused-function \
	-Wno-implicit-fallthrough
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Iexternal/libcxx/include \
	-Iexternal/libcxxabi/include
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++14
LOCAL_RTTI_FLAG := -frtti

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libc++abi
LOCAL_SRC_FILES := \
	external/libcxxabi/src/abort_message.cpp \
	external/libcxxabi/src/cxa_aux_runtime.cpp \
	external/libcxxabi/src/cxa_default_handlers.cpp \
	external/libcxxabi/src/cxa_exception.cpp \
	external/libcxxabi/src/cxa_exception_storage.cpp \
	external/libcxxabi/src/cxa_guard.cpp \
	external/libcxxabi/src/cxa_handlers.cpp \
	external/libcxxabi/src/cxa_personality.cpp \
	external/libcxxabi/src/cxa_thread_atexit.cpp \
	external/libcxxabi/src/cxa_unexpected.cpp \
	external/libcxxabi/src/cxa_vector.cpp \
	external/libcxxabi/src/cxa_virtual.cpp \
	external/libcxxabi/src/fallback_malloc.cpp \
	external/libcxxabi/src/private_typeinfo.cpp \
	external/libcxxabi/src/stdlib_exception.cpp \
	external/libcxxabi/src/stdlib_new_delete.cpp \
	external/libcxxabi/src/stdlib_stdexcept.cpp \
	external/libcxxabi/src/stdlib_typeinfo.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror
LOCAL_CPPFLAGS := \
	-fexceptions \
	-Wextra \
	-Wno-unused-function \
	-Wno-implicit-fallthrough \
	-DHAVE___CXA_THREAD_ATEXIT_IMPL
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Iexternal/libcxx/include \
	-Iexternal/libcxxabi/include
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++14
LOCAL_RTTI_FLAG := -frtti

include static_library.mk
