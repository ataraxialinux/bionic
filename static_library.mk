STEM_TYPE := static_library

include base.mk

linked_module := $(intermediates)/$(notdir $(my_built_module_stem))

LOCAL_INTERMEDIATE_TARGETS := $(linked_module)

include binary.mk

$(linked_module): $(LOCAL_EXTRA_DEPS) $(all_objects)
	$(call transform-o-to-static-lib)
