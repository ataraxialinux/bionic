include defines.mk

linker_cflags = \
	-fno-stack-protector \
	-Wstrict-overflow=5 \
	-fvisibility=hidden \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror

linker_asflags = \
	-fno-stack-protector \
	-Wstrict-overflow=5 \
	-fvisibility=hidden \
	-Wall \
	-Wextra \
	-Wunused \
	-Werror

linker_includes = \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/libziparchive/include \
	-Ibionic/libc/async_safe/include \
	-Ibionic/libc

linker_sources = \
	bionic/linker/dlfcn.cpp \
	bionic/linker/linker.cpp \
	bionic/linker/linker_block_allocator.cpp \
	bionic/linker/linker_dlwarning.cpp \
	bionic/linker/linker_cfi.cpp \
	bionic/linker/linker_config.cpp \
	bionic/linker/linker_debug.cpp \
	bionic/linker/linker_gdb_support.cpp \
	bionic/linker/linker_globals.cpp \
	bionic/linker/linker_libc_support.c \
	bionic/linker/linker_libcxx_support.cpp \
	bionic/linker/linker_namespaces.cpp \
	bionic/linker/linker_logger.cpp \
	bionic/linker/linker_mapped_file_fragment.cpp \
	bionic/linker/linker_note_gnu_property.cpp \
	bionic/linker/linker_phdr.cpp \
	bionic/linker/linker_relocate.cpp \
	bionic/linker/linker_sdk_versions.cpp \
	bionic/linker/linker_soinfo.cpp \
	bionic/linker/linker_transparent_hugepage_support.cpp \
	bionic/linker/linker_tls.cpp \
	bionic/linker/linker_utils.cpp \
	bionic/linker/rt.cpp

linker_sources_arm = \
	bionic/linker/arch/arm/begin.S \
	bionic/linker/arch/arm_neon/linker_gnu_hash_neon.cpp

linker_sources_arm64 = \
	bionic/linker/arch/arm64/begin.S \
	bionic/linker/arch/arm64/tlsdesc_resolver.S \
	bionic/linker/arch/arm_neon/linker_gnu_hash_neon.cpp

linker_sources_x86 = \
	bionic/linker/arch/x86/begin.S

linker_sources_x86_64 = \
	bionic/linker/arch/x86_64/begin.S

ifeq ($(ARCH),arm)
linker_version_script_overlay = bionic/linker/linker.arm.map
else
linker_version_script_overlay = bionic/linker/linker.generic.map
endif

linker_ldflags = \
	-shared \
	-Wl,-Bsymbolic \
	-Wl,--exclude-libs,ALL \
	-Wl,-soname,ld-android.so

include cleanvars.mk

LOCAL_MODULE := liblinker_main
LOCAL_SRC_FILES := bionic/linker/linker_main.cpp
LOCAL_CFLAGS := $(linker_cflags) \
	-ffreestanding
LOCAL_ASFLAGS := $(linker_asflags)
LOCAL_C_INCLUDES := $(linker_includes)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := liblinker_malloc
LOCAL_SRC_FILES := bionic/linker/linker_memory.cpp
LOCAL_CFLAGS := $(linker_cflags)
LOCAL_ASFLAGS := $(linker_asflags)
LOCAL_C_INCLUDES := $(linker_includes)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := liblinker_debuggerd_stub
LOCAL_SRC_FILES := bionic/linker/linker_debuggerd_stub.cpp
LOCAL_CFLAGS := $(linker_cflags)
LOCAL_ASFLAGS := $(linker_asflags)
LOCAL_C_INCLUDES := $(linker_includes)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := linker
LOCAL_SRC_FILES := $(linker_sources) \
	bionic/linker/linker_translate_path.cpp
LOCAL_SRC_FILES_arm := $(linker_sources_arm)
LOCAL_SRC_FILES_arm64 := $(linker_sources_arm64)
LOCAL_SRC_FILES_x86 := $(linker_sources_x86)
LOCAL_SRC_FILES_x86_64 := $(linker_sources_x86_64)
LOCAL_CFLAGS := $(linker_cflags)
LOCAL_ASFLAGS := $(linker_asflags)
LOCAL_C_INCLUDES := $(linker_includes)
LOCAL_LDFLAGS := $(linker_ldflags) \
	-Wl,--version-script,$(linker_version_script_overlay)
LOCAL_EXTRA_DEPS := \
	$(builddir)/libziparchive.a \
	$(builddir)/libbase.a \
	$(builddir)/libz.a \
	$(builddir)/libasync_safe.a \
	$(builddir)/liblog.a \
	$(builddir)/liblinker_main.a \
	$(builddir)/liblinker_malloc.a \
	$(builddir)/liblinker_debuggerd_stub.a \
	$(builddir)/libc++.a \
	$(builddir)/libc_nomalloc.a \
	$(builddir)/libc_dynamic_dispatch.a \
	$(builddir)/libm.a \
	$(builddir)/libc_unwind_static.a \
	$(LIBUNWIND)
LOCAL_STATIC_LIBRARIES := \
	$(builddir)/libziparchive.a \
	$(builddir)/libbase.a \
	$(builddir)/libz.a \
	$(builddir)/libasync_safe.a \
	$(builddir)/liblog.a \
	$(builddir)/liblinker_main.a \
	$(builddir)/liblinker_malloc.a \
	$(builddir)/liblinker_debuggerd_stub.a \
	$(builddir)/libc++.a \
	$(builddir)/libc_nomalloc.a \
	$(builddir)/libc_dynamic_dispatch.a \
	$(builddir)/libm.a \
	$(LIBUNWIND)
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libc_unwind_static.a
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++17
LOCAL_USE_CRT := false
LOCAL_FORCE_STATIC_EXECUTABLE := true

include executable.mk
