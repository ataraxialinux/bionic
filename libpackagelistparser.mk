include defines.mk

include cleanvars.mk

LOCAL_MODULE := libpackagelistparser
LOCAL_SRC_FILES := system/core/libpackagelistparser/packagelistparser.cpp
LOCAL_CFLAGS := \
	-Werror
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/core/libpackagelistparser/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libpackagelistparser
LOCAL_EXTRA_DEPS := \
	$(builddir)/libpackagelistparser.a \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/liblog.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libpackagelistparser.a

include shared_library.mk
