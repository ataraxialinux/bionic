include defines.mk
include funcs.mk

TOPDIR := $(shell pwd)

comma := ,

ifeq ($(STEM_TYPE),static_library)
my_built_module_stem := $(LOCAL_MODULE).a
my_final_local_module := $(LOCAL_MODULE)_static
else ifeq ($(STEM_TYPE),shared_library)
my_built_module_stem := $(LOCAL_MODULE).so
my_final_local_module := $(LOCAL_MODULE)_shared
else ifeq ($(STEM_TYPE),executable)
my_built_module_stem := $(LOCAL_MODULE)
my_final_local_module := $(LOCAL_MODULE)_executable
else
$(error Unknown stem type)
endif

builddir := $(call get-build-dir)
moduleddir := $(call get-module-dir)
intermediates := $(call get-intermediates-dir)

LOCAL_BUILT_MODULE := $(intermediates)/$(my_built_module_stem)

all: $(LOCAL_BUILT_MODULE)
