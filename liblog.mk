include defines.mk

include cleanvars.mk

LOCAL_MODULE := liblog
LOCAL_SRC_FILES := \
	system/logging/liblog/log_event_list.cpp \
	system/logging/liblog/log_event_write.cpp \
	system/logging/liblog/logger_name.cpp \
	system/logging/liblog/logger_read.cpp \
	system/logging/liblog/logger_write.cpp \
	system/logging/liblog/logprint.cpp \
	system/logging/liblog/properties.cpp \
	system/logging/liblog/event_tag_map.cpp \
	system/logging/liblog/log_time.cpp \
	system/logging/liblog/pmsg_reader.cpp \
	system/logging/liblog/pmsg_writer.cpp \
	system/logging/liblog/logd_reader.cpp \
	system/logging/liblog/logd_writer.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wextra \
	-Wexit-time-destructors \
	-DLIBLOG_LOG_TAG=1006 \
	-DSNET_EVENT_LOG_TAG=1397638484
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/logging/liblog/include \
	-Isystem/libbase/include \
	-Isystem/core/libcutils/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := liblog
LOCAL_EXTRA_DEPS := \
	$(builddir)/liblog.a \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/liblog.a
LOCAL_LDFLAGS := -Wl,--version-script,$(builddir)/liblog_shared/objs/system/logging/liblog/liblog.map
LOCAL_LDFLAGS_arm := -Wl,--hash-style=both
LOCAL_API_STUBS := system/logging/liblog/liblog.map.txt

include shared_library.mk
