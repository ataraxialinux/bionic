#!/usr/bin/env sh

set -e

AR="$1"
OUTPUT="$2"
TOPDIR="$3"
DIRECTORY="$4"
ARCHIVES="$5"

REALDIR="$(realpath $DIRECTORY)"
TMPDIR="$(mktemp -d)"

if [ -z "$DIRECTORY" ]; then
	exit 1
fi

if [ ! -d "$REALDIR" ]; then
	exit 1
fi

cd "$REALDIR"

while IFS= read -r file; do
	a="$(echo $file | tr '/' '_')"
	cp $file $TMPDIR/$a
done < <(find . -type f -name "*.o" | sed 's|./||')

cd "$TMPDIR"

if [ -n "$ARCHIVES" ]; then
	for i in $ARCHIVES; do
		$AR x $TOPDIR/$i
	done
fi

if [ -z "$(ls)" ]; then
	$AR crsD -format=gnu $TOPDIR/$OUTPUT
else
	$AR crsD -format=gnu $TOPDIR/$OUTPUT *.o
fi
