include defines.mk

include cleanvars.mk

LOCAL_MODULE := libandroidicuinit
LOCAL_SRC_FILES := \
	external/icu/libandroidicuinit/android_icu_init.cpp \
	external/icu/libandroidicuinit/IcuRegistration.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-DANDROID_LINK_SHARED_ICU4C
LOCAL_C_INCLUDES := \
	-Iexternal/icu/libandroidicuinit/include \
	-Iexternal/icu/android_icu4c/include \
	-Iexternal/icu/icu4c/source/common \
	-Isystem/libbase/include \
	-Iexternal/libcxx/include \
	$(bionicHeaders)

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libicuuc_stubdata
LOCAL_SRC_FILES := external/icu/icu4c/source/stubdata/stubdata.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Wextra \
	-DANDROID_LINK_SHARED_ICU4C
LOCAL_C_INCLUDES := \
	-Iexternal/icu/android_icu4c/include \
	-Iexternal/icu/icu4c/source/common \
	-Iexternal/libcxx/include \
	$(bionicHeaders)
LOCAL_MODULE_NO_STL_STD := true
LOCAL_CXX_STL_VERSION := -std=c++11

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libicuuc
LOCAL_SRC_FILES := \
	external/icu/icu4c/source/common/appendable.cpp \
	external/icu/icu4c/source/common/bmpset.cpp \
	external/icu/icu4c/source/common/brkeng.cpp \
	external/icu/icu4c/source/common/brkiter.cpp \
	external/icu/icu4c/source/common/bytesinkutil.cpp \
	external/icu/icu4c/source/common/bytestream.cpp \
	external/icu/icu4c/source/common/bytestriebuilder.cpp \
	external/icu/icu4c/source/common/bytestrie.cpp \
	external/icu/icu4c/source/common/bytestrieiterator.cpp \
	external/icu/icu4c/source/common/caniter.cpp \
	external/icu/icu4c/source/common/characterproperties.cpp \
	external/icu/icu4c/source/common/chariter.cpp \
	external/icu/icu4c/source/common/charstr.cpp \
	external/icu/icu4c/source/common/cmemory.cpp \
	external/icu/icu4c/source/common/cstr.cpp \
	external/icu/icu4c/source/common/cstring.cpp \
	external/icu/icu4c/source/common/cwchar.cpp \
	external/icu/icu4c/source/common/dictbe.cpp \
	external/icu/icu4c/source/common/dictionarydata.cpp \
	external/icu/icu4c/source/common/dtintrv.cpp \
	external/icu/icu4c/source/common/edits.cpp \
	external/icu/icu4c/source/common/emojiprops.cpp \
	external/icu/icu4c/source/common/errorcode.cpp \
	external/icu/icu4c/source/common/filteredbrk.cpp \
	external/icu/icu4c/source/common/filterednormalizer2.cpp \
	external/icu/icu4c/source/common/icudataver.cpp \
	external/icu/icu4c/source/common/icuplug.cpp \
	external/icu/icu4c/source/common/loadednormalizer2impl.cpp \
	external/icu/icu4c/source/common/localebuilder.cpp \
	external/icu/icu4c/source/common/localematcher.cpp \
	external/icu/icu4c/source/common/localeprioritylist.cpp \
	external/icu/icu4c/source/common/locavailable.cpp \
	external/icu/icu4c/source/common/locbased.cpp \
	external/icu/icu4c/source/common/locdispnames.cpp \
	external/icu/icu4c/source/common/locdistance.cpp \
	external/icu/icu4c/source/common/locdspnm.cpp \
	external/icu/icu4c/source/common/locid.cpp \
	external/icu/icu4c/source/common/loclikely.cpp \
	external/icu/icu4c/source/common/loclikelysubtags.cpp \
	external/icu/icu4c/source/common/locmap.cpp \
	external/icu/icu4c/source/common/locresdata.cpp \
	external/icu/icu4c/source/common/locutil.cpp \
	external/icu/icu4c/source/common/lsr.cpp \
	external/icu/icu4c/source/common/lstmbe.cpp \
	external/icu/icu4c/source/common/messagepattern.cpp \
	external/icu/icu4c/source/common/normalizer2.cpp \
	external/icu/icu4c/source/common/normalizer2impl.cpp \
	external/icu/icu4c/source/common/normlzr.cpp \
	external/icu/icu4c/source/common/parsepos.cpp \
	external/icu/icu4c/source/common/patternprops.cpp \
	external/icu/icu4c/source/common/pluralmap.cpp \
	external/icu/icu4c/source/common/propname.cpp \
	external/icu/icu4c/source/common/propsvec.cpp \
	external/icu/icu4c/source/common/punycode.cpp \
	external/icu/icu4c/source/common/putil.cpp \
	external/icu/icu4c/source/common/rbbi_cache.cpp \
	external/icu/icu4c/source/common/rbbi.cpp \
	external/icu/icu4c/source/common/rbbidata.cpp \
	external/icu/icu4c/source/common/rbbinode.cpp \
	external/icu/icu4c/source/common/rbbirb.cpp \
	external/icu/icu4c/source/common/rbbiscan.cpp \
	external/icu/icu4c/source/common/rbbisetb.cpp \
	external/icu/icu4c/source/common/rbbistbl.cpp \
	external/icu/icu4c/source/common/rbbitblb.cpp \
	external/icu/icu4c/source/common/resbund_cnv.cpp \
	external/icu/icu4c/source/common/resbund.cpp \
	external/icu/icu4c/source/common/resource.cpp \
	external/icu/icu4c/source/common/restrace.cpp \
	external/icu/icu4c/source/common/ruleiter.cpp \
	external/icu/icu4c/source/common/schriter.cpp \
	external/icu/icu4c/source/common/serv.cpp \
	external/icu/icu4c/source/common/servlk.cpp \
	external/icu/icu4c/source/common/servlkf.cpp \
	external/icu/icu4c/source/common/servls.cpp \
	external/icu/icu4c/source/common/servnotf.cpp \
	external/icu/icu4c/source/common/servrbf.cpp \
	external/icu/icu4c/source/common/servslkf.cpp \
	external/icu/icu4c/source/common/sharedobject.cpp \
	external/icu/icu4c/source/common/simpleformatter.cpp \
	external/icu/icu4c/source/common/static_unicode_sets.cpp \
	external/icu/icu4c/source/common/stringpiece.cpp \
	external/icu/icu4c/source/common/stringtriebuilder.cpp \
	external/icu/icu4c/source/common/uarrsort.cpp \
	external/icu/icu4c/source/common/ubidi.cpp \
	external/icu/icu4c/source/common/ubidiln.cpp \
	external/icu/icu4c/source/common/ubidi_props.cpp \
	external/icu/icu4c/source/common/ubiditransform.cpp \
	external/icu/icu4c/source/common/ubidiwrt.cpp \
	external/icu/icu4c/source/common/ubrk.cpp \
	external/icu/icu4c/source/common/ucase.cpp \
	external/icu/icu4c/source/common/ucasemap.cpp \
	external/icu/icu4c/source/common/ucasemap_titlecase_brkiter.cpp \
	external/icu/icu4c/source/common/ucat.cpp \
	external/icu/icu4c/source/common/uchar.cpp \
	external/icu/icu4c/source/common/ucharstriebuilder.cpp \
	external/icu/icu4c/source/common/ucharstrie.cpp \
	external/icu/icu4c/source/common/ucharstrieiterator.cpp \
	external/icu/icu4c/source/common/uchriter.cpp \
	external/icu/icu4c/source/common/ucln_cmn.cpp \
	external/icu/icu4c/source/common/ucmndata.cpp \
	external/icu/icu4c/source/common/ucnv2022.cpp \
	external/icu/icu4c/source/common/ucnv_bld.cpp \
	external/icu/icu4c/source/common/ucnvbocu.cpp \
	external/icu/icu4c/source/common/ucnv_cb.cpp \
	external/icu/icu4c/source/common/ucnv_cnv.cpp \
	external/icu/icu4c/source/common/ucnv.cpp \
	external/icu/icu4c/source/common/ucnv_ct.cpp \
	external/icu/icu4c/source/common/ucnvdisp.cpp \
	external/icu/icu4c/source/common/ucnv_err.cpp \
	external/icu/icu4c/source/common/ucnv_ext.cpp \
	external/icu/icu4c/source/common/ucnvhz.cpp \
	external/icu/icu4c/source/common/ucnv_io.cpp \
	external/icu/icu4c/source/common/ucnvisci.cpp \
	external/icu/icu4c/source/common/ucnvlat1.cpp \
	external/icu/icu4c/source/common/ucnv_lmb.cpp \
	external/icu/icu4c/source/common/ucnvmbcs.cpp \
	external/icu/icu4c/source/common/ucnvscsu.cpp \
	external/icu/icu4c/source/common/ucnvsel.cpp \
	external/icu/icu4c/source/common/ucnv_set.cpp \
	external/icu/icu4c/source/common/ucnv_u16.cpp \
	external/icu/icu4c/source/common/ucnv_u32.cpp \
	external/icu/icu4c/source/common/ucnv_u7.cpp \
	external/icu/icu4c/source/common/ucnv_u8.cpp \
	external/icu/icu4c/source/common/ucol_swp.cpp \
	external/icu/icu4c/source/common/ucptrie.cpp \
	external/icu/icu4c/source/common/ucurr.cpp \
	external/icu/icu4c/source/common/udata.cpp \
	external/icu/icu4c/source/common/udatamem.cpp \
	external/icu/icu4c/source/common/udataswp.cpp \
	external/icu/icu4c/source/common/uenum.cpp \
	external/icu/icu4c/source/common/uhash.cpp \
	external/icu/icu4c/source/common/uhash_us.cpp \
	external/icu/icu4c/source/common/uidna.cpp \
	external/icu/icu4c/source/common/uinit.cpp \
	external/icu/icu4c/source/common/uinvchar.cpp \
	external/icu/icu4c/source/common/uiter.cpp \
	external/icu/icu4c/source/common/ulist.cpp \
	external/icu/icu4c/source/common/uloc.cpp \
	external/icu/icu4c/source/common/uloc_keytype.cpp \
	external/icu/icu4c/source/common/uloc_tag.cpp \
	external/icu/icu4c/source/common/umapfile.cpp \
	external/icu/icu4c/source/common/umath.cpp \
	external/icu/icu4c/source/common/umutablecptrie.cpp \
	external/icu/icu4c/source/common/umutex.cpp \
	external/icu/icu4c/source/common/unames.cpp \
	external/icu/icu4c/source/common/unifiedcache.cpp \
	external/icu/icu4c/source/common/unifilt.cpp \
	external/icu/icu4c/source/common/unifunct.cpp \
	external/icu/icu4c/source/common/uniset_closure.cpp \
	external/icu/icu4c/source/common/uniset.cpp \
	external/icu/icu4c/source/common/uniset_props.cpp \
	external/icu/icu4c/source/common/unisetspan.cpp \
	external/icu/icu4c/source/common/unistr_case.cpp \
	external/icu/icu4c/source/common/unistr_case_locale.cpp \
	external/icu/icu4c/source/common/unistr_cnv.cpp \
	external/icu/icu4c/source/common/unistr.cpp \
	external/icu/icu4c/source/common/unistr_props.cpp \
	external/icu/icu4c/source/common/unistr_titlecase_brkiter.cpp \
	external/icu/icu4c/source/common/unormcmp.cpp \
	external/icu/icu4c/source/common/unorm.cpp \
	external/icu/icu4c/source/common/uobject.cpp \
	external/icu/icu4c/source/common/uprops.cpp \
	external/icu/icu4c/source/common/uresbund.cpp \
	external/icu/icu4c/source/common/ures_cnv.cpp \
	external/icu/icu4c/source/common/uresdata.cpp \
	external/icu/icu4c/source/common/usc_impl.cpp \
	external/icu/icu4c/source/common/uscript.cpp \
	external/icu/icu4c/source/common/uscript_props.cpp \
	external/icu/icu4c/source/common/uset.cpp \
	external/icu/icu4c/source/common/usetiter.cpp \
	external/icu/icu4c/source/common/uset_props.cpp \
	external/icu/icu4c/source/common/ushape.cpp \
	external/icu/icu4c/source/common/usprep.cpp \
	external/icu/icu4c/source/common/ustack.cpp \
	external/icu/icu4c/source/common/ustrcase.cpp \
	external/icu/icu4c/source/common/ustrcase_locale.cpp \
	external/icu/icu4c/source/common/ustr_cnv.cpp \
	external/icu/icu4c/source/common/ustrenum.cpp \
	external/icu/icu4c/source/common/ustrfmt.cpp \
	external/icu/icu4c/source/common/ustring.cpp \
	external/icu/icu4c/source/common/ustr_titlecase_brkiter.cpp \
	external/icu/icu4c/source/common/ustrtrns.cpp \
	external/icu/icu4c/source/common/ustr_wcs.cpp \
	external/icu/icu4c/source/common/utext.cpp \
	external/icu/icu4c/source/common/utf_impl.cpp \
	external/icu/icu4c/source/common/util.cpp \
	external/icu/icu4c/source/common/util_props.cpp \
	external/icu/icu4c/source/common/utrace.cpp \
	external/icu/icu4c/source/common/utrie2_builder.cpp \
	external/icu/icu4c/source/common/utrie2.cpp \
	external/icu/icu4c/source/common/utrie.cpp \
	external/icu/icu4c/source/common/utrie_swap.cpp \
	external/icu/icu4c/source/common/uts46.cpp \
	external/icu/icu4c/source/common/utypes.cpp \
	external/icu/icu4c/source/common/uvector.cpp \
	external/icu/icu4c/source/common/uvectr32.cpp \
	external/icu/icu4c/source/common/uvectr64.cpp \
	external/icu/icu4c/source/common/wintz.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter \
	-Wno-unused-const-variable \
	-Wno-unneeded-internal-declaration \
	-Wno-deprecated-declarations \
	-DANDROID_LINK_SHARED_ICU4C \
	-D_REENTRANT \
	-DU_COMMON_IMPLEMENTATION \
	-O3 \
	-fvisibility=hidden \
	-Wno-missing-field-initializers \
	-Wno-sign-compare \
	-DPIC \
	-fPIC
LOCAL_C_INCLUDES := \
	-Iexternal/icu/android_icu4c/include \
	-Iexternal/icu/libandroidicuinit/include \
	-Iexternal/icu/icu4c/source/common \
	-Iexternal/libcxx/include \
	$(bionicHeaders)
LOCAL_CXX_STL_VERSION := -std=c++11
LOCAL_RTTI_FLAG := -frtti

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libicui18n
LOCAL_SRC_FILES := \
	external/icu/icu4c/source/i18n/alphaindex.cpp \
	external/icu/icu4c/source/i18n/anytrans.cpp \
	external/icu/icu4c/source/i18n/astro.cpp \
	external/icu/icu4c/source/i18n/basictz.cpp \
	external/icu/icu4c/source/i18n/bocsu.cpp \
	external/icu/icu4c/source/i18n/brktrans.cpp \
	external/icu/icu4c/source/i18n/buddhcal.cpp \
	external/icu/icu4c/source/i18n/calendar.cpp \
	external/icu/icu4c/source/i18n/casetrn.cpp \
	external/icu/icu4c/source/i18n/cecal.cpp \
	external/icu/icu4c/source/i18n/chnsecal.cpp \
	external/icu/icu4c/source/i18n/choicfmt.cpp \
	external/icu/icu4c/source/i18n/coleitr.cpp \
	external/icu/icu4c/source/i18n/collationbuilder.cpp \
	external/icu/icu4c/source/i18n/collationcompare.cpp \
	external/icu/icu4c/source/i18n/collation.cpp \
	external/icu/icu4c/source/i18n/collationdatabuilder.cpp \
	external/icu/icu4c/source/i18n/collationdata.cpp \
	external/icu/icu4c/source/i18n/collationdatareader.cpp \
	external/icu/icu4c/source/i18n/collationdatawriter.cpp \
	external/icu/icu4c/source/i18n/collationfastlatinbuilder.cpp \
	external/icu/icu4c/source/i18n/collationfastlatin.cpp \
	external/icu/icu4c/source/i18n/collationfcd.cpp \
	external/icu/icu4c/source/i18n/collationiterator.cpp \
	external/icu/icu4c/source/i18n/collationkeys.cpp \
	external/icu/icu4c/source/i18n/collationroot.cpp \
	external/icu/icu4c/source/i18n/collationrootelements.cpp \
	external/icu/icu4c/source/i18n/collationruleparser.cpp \
	external/icu/icu4c/source/i18n/collationsets.cpp \
	external/icu/icu4c/source/i18n/collationsettings.cpp \
	external/icu/icu4c/source/i18n/collationtailoring.cpp \
	external/icu/icu4c/source/i18n/collationweights.cpp \
	external/icu/icu4c/source/i18n/coll.cpp \
	external/icu/icu4c/source/i18n/compactdecimalformat.cpp \
	external/icu/icu4c/source/i18n/coptccal.cpp \
	external/icu/icu4c/source/i18n/cpdtrans.cpp \
	external/icu/icu4c/source/i18n/csdetect.cpp \
	external/icu/icu4c/source/i18n/csmatch.cpp \
	external/icu/icu4c/source/i18n/csr2022.cpp \
	external/icu/icu4c/source/i18n/csrecog.cpp \
	external/icu/icu4c/source/i18n/csrmbcs.cpp \
	external/icu/icu4c/source/i18n/csrsbcs.cpp \
	external/icu/icu4c/source/i18n/csrucode.cpp \
	external/icu/icu4c/source/i18n/csrutf8.cpp \
	external/icu/icu4c/source/i18n/curramt.cpp \
	external/icu/icu4c/source/i18n/currfmt.cpp \
	external/icu/icu4c/source/i18n/currpinf.cpp \
	external/icu/icu4c/source/i18n/currunit.cpp \
	external/icu/icu4c/source/i18n/dangical.cpp \
	external/icu/icu4c/source/i18n/datefmt.cpp \
	external/icu/icu4c/source/i18n/dayperiodrules.cpp \
	external/icu/icu4c/source/i18n/dcfmtsym.cpp \
	external/icu/icu4c/source/i18n/decContext.cpp \
	external/icu/icu4c/source/i18n/decimfmt.cpp \
	external/icu/icu4c/source/i18n/decNumber.cpp \
	external/icu/icu4c/source/i18n/double-conversion-bignum.cpp \
	external/icu/icu4c/source/i18n/double-conversion-bignum-dtoa.cpp \
	external/icu/icu4c/source/i18n/double-conversion-cached-powers.cpp \
	external/icu/icu4c/source/i18n/double-conversion-double-to-string.cpp \
	external/icu/icu4c/source/i18n/double-conversion-fast-dtoa.cpp \
	external/icu/icu4c/source/i18n/double-conversion-string-to-double.cpp \
	external/icu/icu4c/source/i18n/double-conversion-strtod.cpp \
	external/icu/icu4c/source/i18n/dtfmtsym.cpp \
	external/icu/icu4c/source/i18n/dtitvfmt.cpp \
	external/icu/icu4c/source/i18n/dtitvinf.cpp \
	external/icu/icu4c/source/i18n/dtptngen.cpp \
	external/icu/icu4c/source/i18n/dtrule.cpp \
	external/icu/icu4c/source/i18n/erarules.cpp \
	external/icu/icu4c/source/i18n/esctrn.cpp \
	external/icu/icu4c/source/i18n/ethpccal.cpp \
	external/icu/icu4c/source/i18n/fmtable_cnv.cpp \
	external/icu/icu4c/source/i18n/fmtable.cpp \
	external/icu/icu4c/source/i18n/format.cpp \
	external/icu/icu4c/source/i18n/formatted_string_builder.cpp \
	external/icu/icu4c/source/i18n/formattedval_iterimpl.cpp \
	external/icu/icu4c/source/i18n/formattedval_sbimpl.cpp \
	external/icu/icu4c/source/i18n/formattedvalue.cpp \
	external/icu/icu4c/source/i18n/fphdlimp.cpp \
	external/icu/icu4c/source/i18n/fpositer.cpp \
	external/icu/icu4c/source/i18n/funcrepl.cpp \
	external/icu/icu4c/source/i18n/gender.cpp \
	external/icu/icu4c/source/i18n/gregocal.cpp \
	external/icu/icu4c/source/i18n/gregoimp.cpp \
	external/icu/icu4c/source/i18n/hebrwcal.cpp \
	external/icu/icu4c/source/i18n/indiancal.cpp \
	external/icu/icu4c/source/i18n/inputext.cpp \
	external/icu/icu4c/source/i18n/islamcal.cpp \
	external/icu/icu4c/source/i18n/japancal.cpp \
	external/icu/icu4c/source/i18n/listformatter.cpp \
	external/icu/icu4c/source/i18n/measfmt.cpp \
	external/icu/icu4c/source/i18n/measunit.cpp \
	external/icu/icu4c/source/i18n/measunit_extra.cpp \
	external/icu/icu4c/source/i18n/measure.cpp \
	external/icu/icu4c/source/i18n/msgfmt.cpp \
	external/icu/icu4c/source/i18n/name2uni.cpp \
	external/icu/icu4c/source/i18n/nfrs.cpp \
	external/icu/icu4c/source/i18n/nfrule.cpp \
	external/icu/icu4c/source/i18n/nfsubs.cpp \
	external/icu/icu4c/source/i18n/nortrans.cpp \
	external/icu/icu4c/source/i18n/nultrans.cpp \
	external/icu/icu4c/source/i18n/number_affixutils.cpp \
	external/icu/icu4c/source/i18n/number_asformat.cpp \
	external/icu/icu4c/source/i18n/number_capi.cpp \
	external/icu/icu4c/source/i18n/number_compact.cpp \
	external/icu/icu4c/source/i18n/number_currencysymbols.cpp \
	external/icu/icu4c/source/i18n/number_decimalquantity.cpp \
	external/icu/icu4c/source/i18n/number_decimfmtprops.cpp \
	external/icu/icu4c/source/i18n/number_fluent.cpp \
	external/icu/icu4c/source/i18n/number_formatimpl.cpp \
	external/icu/icu4c/source/i18n/number_grouping.cpp \
	external/icu/icu4c/source/i18n/number_integerwidth.cpp \
	external/icu/icu4c/source/i18n/number_longnames.cpp \
	external/icu/icu4c/source/i18n/number_mapper.cpp \
	external/icu/icu4c/source/i18n/number_modifiers.cpp \
	external/icu/icu4c/source/i18n/number_multiplier.cpp \
	external/icu/icu4c/source/i18n/number_notation.cpp \
	external/icu/icu4c/source/i18n/number_output.cpp \
	external/icu/icu4c/source/i18n/number_padding.cpp \
	external/icu/icu4c/source/i18n/number_patternmodifier.cpp \
	external/icu/icu4c/source/i18n/number_patternstring.cpp \
	external/icu/icu4c/source/i18n/number_rounding.cpp \
	external/icu/icu4c/source/i18n/number_scientific.cpp \
	external/icu/icu4c/source/i18n/number_skeletons.cpp \
	external/icu/icu4c/source/i18n/number_symbolswrapper.cpp \
	external/icu/icu4c/source/i18n/number_usageprefs.cpp \
	external/icu/icu4c/source/i18n/number_utils.cpp \
	external/icu/icu4c/source/i18n/numfmt.cpp \
	external/icu/icu4c/source/i18n/numparse_affixes.cpp \
	external/icu/icu4c/source/i18n/numparse_compositions.cpp \
	external/icu/icu4c/source/i18n/numparse_currency.cpp \
	external/icu/icu4c/source/i18n/numparse_decimal.cpp \
	external/icu/icu4c/source/i18n/numparse_impl.cpp \
	external/icu/icu4c/source/i18n/numparse_parsednumber.cpp \
	external/icu/icu4c/source/i18n/numparse_scientific.cpp \
	external/icu/icu4c/source/i18n/numparse_symbols.cpp \
	external/icu/icu4c/source/i18n/numparse_validators.cpp \
	external/icu/icu4c/source/i18n/numrange_capi.cpp \
	external/icu/icu4c/source/i18n/numrange_fluent.cpp \
	external/icu/icu4c/source/i18n/numrange_impl.cpp \
	external/icu/icu4c/source/i18n/numsys.cpp \
	external/icu/icu4c/source/i18n/olsontz.cpp \
	external/icu/icu4c/source/i18n/persncal.cpp \
	external/icu/icu4c/source/i18n/pluralranges.cpp \
	external/icu/icu4c/source/i18n/plurfmt.cpp \
	external/icu/icu4c/source/i18n/plurrule.cpp \
	external/icu/icu4c/source/i18n/quant.cpp \
	external/icu/icu4c/source/i18n/quantityformatter.cpp \
	external/icu/icu4c/source/i18n/rbnf.cpp \
	external/icu/icu4c/source/i18n/rbt.cpp \
	external/icu/icu4c/source/i18n/rbt_data.cpp \
	external/icu/icu4c/source/i18n/rbt_pars.cpp \
	external/icu/icu4c/source/i18n/rbt_rule.cpp \
	external/icu/icu4c/source/i18n/rbt_set.cpp \
	external/icu/icu4c/source/i18n/rbtz.cpp \
	external/icu/icu4c/source/i18n/regexcmp.cpp \
	external/icu/icu4c/source/i18n/regeximp.cpp \
	external/icu/icu4c/source/i18n/regexst.cpp \
	external/icu/icu4c/source/i18n/regextxt.cpp \
	external/icu/icu4c/source/i18n/region.cpp \
	external/icu/icu4c/source/i18n/reldatefmt.cpp \
	external/icu/icu4c/source/i18n/reldtfmt.cpp \
	external/icu/icu4c/source/i18n/rematch.cpp \
	external/icu/icu4c/source/i18n/remtrans.cpp \
	external/icu/icu4c/source/i18n/repattrn.cpp \
	external/icu/icu4c/source/i18n/rulebasedcollator.cpp \
	external/icu/icu4c/source/i18n/scientificnumberformatter.cpp \
	external/icu/icu4c/source/i18n/scriptset.cpp \
	external/icu/icu4c/source/i18n/search.cpp \
	external/icu/icu4c/source/i18n/selfmt.cpp \
	external/icu/icu4c/source/i18n/sharedbreakiterator.cpp \
	external/icu/icu4c/source/i18n/simpletz.cpp \
	external/icu/icu4c/source/i18n/smpdtfmt.cpp \
	external/icu/icu4c/source/i18n/smpdtfst.cpp \
	external/icu/icu4c/source/i18n/sortkey.cpp \
	external/icu/icu4c/source/i18n/standardplural.cpp \
	external/icu/icu4c/source/i18n/string_segment.cpp \
	external/icu/icu4c/source/i18n/strmatch.cpp \
	external/icu/icu4c/source/i18n/strrepl.cpp \
	external/icu/icu4c/source/i18n/stsearch.cpp \
	external/icu/icu4c/source/i18n/taiwncal.cpp \
	external/icu/icu4c/source/i18n/timezone.cpp \
	external/icu/icu4c/source/i18n/titletrn.cpp \
	external/icu/icu4c/source/i18n/tmunit.cpp \
	external/icu/icu4c/source/i18n/tmutamt.cpp \
	external/icu/icu4c/source/i18n/tmutfmt.cpp \
	external/icu/icu4c/source/i18n/tolowtrn.cpp \
	external/icu/icu4c/source/i18n/toupptrn.cpp \
	external/icu/icu4c/source/i18n/translit.cpp \
	external/icu/icu4c/source/i18n/transreg.cpp \
	external/icu/icu4c/source/i18n/tridpars.cpp \
	external/icu/icu4c/source/i18n/tzfmt.cpp \
	external/icu/icu4c/source/i18n/tzgnames.cpp \
	external/icu/icu4c/source/i18n/tznames.cpp \
	external/icu/icu4c/source/i18n/tznames_impl.cpp \
	external/icu/icu4c/source/i18n/tzrule.cpp \
	external/icu/icu4c/source/i18n/tztrans.cpp \
	external/icu/icu4c/source/i18n/ucal.cpp \
	external/icu/icu4c/source/i18n/ucln_in.cpp \
	external/icu/icu4c/source/i18n/ucol.cpp \
	external/icu/icu4c/source/i18n/ucoleitr.cpp \
	external/icu/icu4c/source/i18n/ucol_res.cpp \
	external/icu/icu4c/source/i18n/ucol_sit.cpp \
	external/icu/icu4c/source/i18n/ucsdet.cpp \
	external/icu/icu4c/source/i18n/udat.cpp \
	external/icu/icu4c/source/i18n/udateintervalformat.cpp \
	external/icu/icu4c/source/i18n/udatpg.cpp \
	external/icu/icu4c/source/i18n/ufieldpositer.cpp \
	external/icu/icu4c/source/i18n/uitercollationiterator.cpp \
	external/icu/icu4c/source/i18n/ulistformatter.cpp \
	external/icu/icu4c/source/i18n/ulocdata.cpp \
	external/icu/icu4c/source/i18n/umsg.cpp \
	external/icu/icu4c/source/i18n/unesctrn.cpp \
	external/icu/icu4c/source/i18n/uni2name.cpp \
	external/icu/icu4c/source/i18n/units_complexconverter.cpp \
	external/icu/icu4c/source/i18n/units_converter.cpp \
	external/icu/icu4c/source/i18n/units_data.cpp \
	external/icu/icu4c/source/i18n/units_router.cpp \
	external/icu/icu4c/source/i18n/unum.cpp \
	external/icu/icu4c/source/i18n/unumsys.cpp \
	external/icu/icu4c/source/i18n/upluralrules.cpp \
	external/icu/icu4c/source/i18n/uregexc.cpp \
	external/icu/icu4c/source/i18n/uregex.cpp \
	external/icu/icu4c/source/i18n/uregion.cpp \
	external/icu/icu4c/source/i18n/usearch.cpp \
	external/icu/icu4c/source/i18n/uspoof_build.cpp \
	external/icu/icu4c/source/i18n/uspoof_conf.cpp \
	external/icu/icu4c/source/i18n/uspoof.cpp \
	external/icu/icu4c/source/i18n/uspoof_impl.cpp \
	external/icu/icu4c/source/i18n/utf16collationiterator.cpp \
	external/icu/icu4c/source/i18n/utf8collationiterator.cpp \
	external/icu/icu4c/source/i18n/utmscale.cpp \
	external/icu/icu4c/source/i18n/utrans.cpp \
	external/icu/icu4c/source/i18n/vtzone.cpp \
	external/icu/icu4c/source/i18n/vzone.cpp \
	external/icu/icu4c/source/i18n/windtfmt.cpp \
	external/icu/icu4c/source/i18n/winnmfmt.cpp \
	external/icu/icu4c/source/i18n/wintzimpl.cpp \
	external/icu/icu4c/source/i18n/zonemeta.cpp \
	external/icu/icu4c/source/i18n/zrule.cpp \
	external/icu/icu4c/source/i18n/ztrans.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter \
	-Wno-unused-const-variable \
	-Wno-unneeded-internal-declaration \
	-Wno-deprecated-declarations \
	-DANDROID_LINK_SHARED_ICU4C \
	-D_REENTRANT \
	-DU_I18N_IMPLEMENTATION \
	-O3 \
	-fvisibility=hidden \
	-Wno-unreachable-code-loop-increment \
	-DPIC \
	-fPIC
LOCAL_C_INCLUDES := \
	-Iexternal/icu/android_icu4c/include \
	-Iexternal/icu/icu4c/source/common \
	-Iexternal/icu/icu4c/source/i18n \
	-Iexternal/libcxx/include \
	$(bionicHeaders)
LOCAL_CXX_STL_VERSION := -std=c++11
LOCAL_RTTI_FLAG := -frtti

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libicu_static
LOCAL_SRC_FILES := \
	external/icu/libicu/src/shim.cpp
LOCAL_CFLAGS := \
	-Wall \
	-Werror \
	-Wno-unused-parameter \
	-Wno-unused-const-variable \
	-Wno-unneeded-internal-declaration \
	-Wno-deprecated-declarations \
	-DANDROID_LINK_SHARED_ICU4C \
	-DU_SHOW_CPLUSPLUS_API=0
LOCAL_C_INCLUDES := \
	-Iexternal/icu/android_icu4c/include \
	-Iexternal/icu/icu4c/source/common \
	-Iexternal/icu/icu4c/source/i18n \
	-Iexternal/libcxx/include \
	$(bionicHeaders)
LOCAL_CXX_STL_VERSION := -std=c++11

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libicuuc
LOCAL_EXTRA_DEPS := \
	$(builddir)/libicuuc.a \
	$(builddir)/libandroidicuinit.a \
	$(builddir)/libicuuc_stubdata.a \
	$(builddir)/libbase.so \
	$(builddir)/liblog.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/liblog.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_STATIC_LIBRARIES := \
	$(builddir)/libandroidicuinit.a \
	$(builddir)/libicuuc_stubdata.a
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libicuuc.a

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libicui18n
LOCAL_EXTRA_DEPS := \
	$(builddir)/libicui18n.a \
	$(builddir)/libicuuc.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libicuuc.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libicui18n.a

include shared_library.mk

include cleanvars.mk

LOCAL_MODULE := libicu
LOCAL_EXTRA_DEPS := \
	$(builddir)/libicu_static.a \
	$(builddir)/libicuuc.so \
	$(builddir)/libicui18n.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libicuuc.so \
	$(builddir)/libicui18n.so \
	$(builddir)/libc++.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libicu_static.a
LOCAL_LDFLAGS := \
	-Wl,--version-script,$(builddir)/libicu_shared/objs/external/icu/libicu/libicu.map
LOCAL_API_STUBS := external/icu/libicu/libicu.map.txt

include shared_library.mk
