include cleanvars.mk
include defines.mk

LOCAL_MODULE := libscudo
LOCAL_SRC_FILES := \
	external/scudo/standalone/checksum.cpp \
	external/scudo/standalone/common.cpp \
	external/scudo/standalone/flags.cpp \
	external/scudo/standalone/flags_parser.cpp \
	external/scudo/standalone/linux.cpp \
	external/scudo/standalone/release.cpp \
	external/scudo/standalone/report.cpp \
	external/scudo/standalone/string_utils.cpp \
	external/scudo/standalone/wrappers_c_bionic.cpp
LOCAL_SRC_FILES_arm := external/scudo/standalone/crc32_hw.cpp
LOCAL_SRC_FILES_arm64 := external/scudo/standalone/crc32_hw.cpp
LOCAL_SRC_FILES_x86 := external/scudo/standalone/crc32_hw.cpp
LOCAL_SRC_FILES_x86_64 := external/scudo/standalone/crc32_hw.cpp
LOCAL_CFLAGS := \
	-O3 \
	-fno-rtti \
	-fno-stack-protector \
	-fno-emulated-tls \
	-Wall \
	-Wextra \
	-Wunused \
	-Wno-unused-result \
	-Werror=pointer-to-int-cast \
	-Werror=int-to-pointer-cast \
	-Werror=type-limits \
	-Werror \
	-DSCUDO_MIN_ALIGNMENT_LOG=4 \
	-DHAVE_ANDROID_UNSAFE_FRAME_POINTER_CHASE \
	-D_BIONIC=1 \
	-DSCUDO_HAS_PLATFORM_TLS_SLOT
LOCAL_CFLAGS_arm := -mcrc
LOCAL_CFLAGS_arm64 := -mcrc
LOCAL_CFLAGS_x86 := -msse4.2
LOCAL_CFLAGS_x86_64 := -msse4.2
LOCAL_CPPFLAGS := \
	-nostdinc++ \
	-fno-exceptions
LOCAL_C_INCLUDES := -Iexternal/scudo/standalone/include $(bionicPlatformHeaders)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk
