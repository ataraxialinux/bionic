include defines.mk

arm_optimized_routines_defaults_cflags := \
	-Werror \
	-Wno-unused-parameter \
	-O2 \
	-ffp-contract=fast \
	-fno-math-errno \
	-D__BIONIC_LP32_USE_LONG_DOUBLE \
	-DFLT_EVAL_METHOD=0 \
	-DWANT_ERRNO=0 \
	-DWANT_VMATH=0

arm_optimized_routines_defaults_includes := \
	$(bionicHeaders) \
	-Iexternal/arm-optimized-routines/math/include

include cleanvars.mk

LOCAL_MODULE := libarm-optimized-routines-math
LOCAL_SRC_FILES := \
	external/arm-optimized-routines/math/cosf.c \
	external/arm-optimized-routines/math/erf.c \
	external/arm-optimized-routines/math/erf_data.c \
	external/arm-optimized-routines/math/erff.c \
	external/arm-optimized-routines/math/erff_data.c \
	external/arm-optimized-routines/math/exp2.c \
	external/arm-optimized-routines/math/exp2f.c \
	external/arm-optimized-routines/math/exp2f_data.c \
	external/arm-optimized-routines/math/exp.c \
	external/arm-optimized-routines/math/exp_data.c \
	external/arm-optimized-routines/math/expf.c \
	external/arm-optimized-routines/math/log2.c \
	external/arm-optimized-routines/math/log2_data.c \
	external/arm-optimized-routines/math/log2f.c \
	external/arm-optimized-routines/math/log2f_data.c \
	external/arm-optimized-routines/math/log.c \
	external/arm-optimized-routines/math/log_data.c \
	external/arm-optimized-routines/math/logf.c \
	external/arm-optimized-routines/math/logf_data.c \
	external/arm-optimized-routines/math/math_err.c \
	external/arm-optimized-routines/math/math_errf.c \
	external/arm-optimized-routines/math/pow.c \
	external/arm-optimized-routines/math/powf.c \
	external/arm-optimized-routines/math/powf_log2_data.c \
	external/arm-optimized-routines/math/pow_log_data.c \
	external/arm-optimized-routines/math/s_cos.c \
	external/arm-optimized-routines/math/s_cosf.c \
	external/arm-optimized-routines/math/s_exp2f_1u.c \
	external/arm-optimized-routines/math/s_exp2f.c \
	external/arm-optimized-routines/math/s_exp.c \
	external/arm-optimized-routines/math/s_expf_1u.c \
	external/arm-optimized-routines/math/s_expf.c \
	external/arm-optimized-routines/math/sincosf.c \
	external/arm-optimized-routines/math/sincosf_data.c \
	external/arm-optimized-routines/math/sinf.c \
	external/arm-optimized-routines/math/s_log.c \
	external/arm-optimized-routines/math/s_logf.c \
	external/arm-optimized-routines/math/s_pow.c \
	external/arm-optimized-routines/math/s_powf.c \
	external/arm-optimized-routines/math/s_sin.c \
	external/arm-optimized-routines/math/s_sinf.c \
	external/arm-optimized-routines/math/v_cos.c \
	external/arm-optimized-routines/math/v_cosf.c \
	external/arm-optimized-routines/math/v_exp2f_1u.c \
	external/arm-optimized-routines/math/v_exp2f.c \
	external/arm-optimized-routines/math/v_exp.c \
	external/arm-optimized-routines/math/v_exp_data.c \
	external/arm-optimized-routines/math/v_expf_1u.c \
	external/arm-optimized-routines/math/v_expf.c \
	external/arm-optimized-routines/math/v_log.c \
	external/arm-optimized-routines/math/v_log_data.c \
	external/arm-optimized-routines/math/v_logf.c \
	external/arm-optimized-routines/math/vn_cos.c \
	external/arm-optimized-routines/math/vn_cosf.c \
	external/arm-optimized-routines/math/vn_exp2f_1u.c \
	external/arm-optimized-routines/math/vn_exp2f.c \
	external/arm-optimized-routines/math/vn_exp.c \
	external/arm-optimized-routines/math/vn_expf_1u.c \
	external/arm-optimized-routines/math/vn_expf.c \
	external/arm-optimized-routines/math/vn_log.c \
	external/arm-optimized-routines/math/vn_logf.c \
	external/arm-optimized-routines/math/vn_pow.c \
	external/arm-optimized-routines/math/vn_powf.c \
	external/arm-optimized-routines/math/vn_sin.c \
	external/arm-optimized-routines/math/vn_sinf.c \
	external/arm-optimized-routines/math/v_pow.c \
	external/arm-optimized-routines/math/v_powf.c \
	external/arm-optimized-routines/math/v_sin.c \
	external/arm-optimized-routines/math/v_sinf.c
LOCAL_SRC_FILES_EXCLUDE := \
	external/arm-optimized-routines/math/erf.c \
	external/arm-optimized-routines/math/erf_data.c \
	external/arm-optimized-routines/math/erff.c \
	external/arm-optimized-routines/math/erff_data.c
LOCAL_CFLAGS := $(arm_optimized_routines_defaults_cflags)
LOCAL_CFLAGS_arm64 := -DHAVE_FAST_FMA=1
LOCAL_C_INCLUDES := $(arm_optimized_routines_defaults_includes)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libarm-optimized-routines-string
LOCAL_SRC_FILES_arm64 := \
	external/arm-optimized-routines/string/aarch64/memchr-mte.S \
	external/arm-optimized-routines/string/aarch64/memchr.S \
	external/arm-optimized-routines/string/aarch64/memcmp.S \
	external/arm-optimized-routines/string/aarch64/memrchr.S \
	external/arm-optimized-routines/string/aarch64/stpcpy-mte.S \
	external/arm-optimized-routines/string/aarch64/stpcpy.S \
	external/arm-optimized-routines/string/aarch64/strchrnul-mte.S \
	external/arm-optimized-routines/string/aarch64/strchrnul.S \
	external/arm-optimized-routines/string/aarch64/strchr-mte.S \
	external/arm-optimized-routines/string/aarch64/strchr.S \
	external/arm-optimized-routines/string/aarch64/strcmp-mte.S \
	external/arm-optimized-routines/string/aarch64/strcmp.S \
	external/arm-optimized-routines/string/aarch64/strcpy-mte.S \
	external/arm-optimized-routines/string/aarch64/strcpy.S \
	external/arm-optimized-routines/string/aarch64/strlen-mte.S \
	external/arm-optimized-routines/string/aarch64/strlen.S \
	external/arm-optimized-routines/string/aarch64/strncmp-mte.S \
	external/arm-optimized-routines/string/aarch64/strncmp.S \
	external/arm-optimized-routines/string/aarch64/strnlen.S \
	external/arm-optimized-routines/string/aarch64/strrchr-mte.S \
	external/arm-optimized-routines/string/aarch64/strrchr.S
LOCAL_ASFLAGS_arm64 := \
	-D__memcmp_aarch64=memcmp \
	-D__memrchr_aarch64=memrchr \
	-D__strnlen_aarch64=strnlen
LOCAL_C_INCLUDES := $(arm_optimized_routines_defaults_includes)
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk
