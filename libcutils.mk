include defines.mk

include cleanvars.mk

LOCAL_MODULE := libcutils_sockets
LOCAL_SRC_FILES := \
	system/core/libcutils/sockets.cpp \
	system/core/libcutils/android_get_control_file.cpp \
	system/core/libcutils/socket_inaddr_any_server_unix.cpp \
	system/core/libcutils/socket_local_client_unix.cpp \
	system/core/libcutils/socket_local_server_unix.cpp \
	system/core/libcutils/socket_network_client_unix.cpp \
	system/core/libcutils/sockets_unix.cpp
LOCAL_CFLAGS := \
	-Wno-exit-time-destructors
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/core/libcutils/include

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libcutils
LOCAL_SRC_FILES := \
	system/core/libcutils/config_utils.cpp \
	system/core/libcutils/canned_fs_config.cpp \
	system/core/libcutils/iosched_policy.cpp \
	system/core/libcutils/load_file.cpp \
	system/core/libcutils/native_handle.cpp \
	system/core/libcutils/properties.cpp \
	system/core/libcutils/record_stream.cpp \
	system/core/libcutils/strlcpy.c \
	system/core/libcutils/threads.cpp \
	system/core/libcutils/fs.cpp \
	system/core/libcutils/hashmap.cpp \
	system/core/libcutils/multiuser.cpp \
	system/core/libcutils/str_parms.cpp \
	system/core/libcutils/android_reboot.cpp \
	system/core/libcutils/ashmem-dev.cpp \
	system/core/libcutils/fs_config.cpp \
	system/core/libcutils/klog.cpp \
	system/core/libcutils/partition_utils.cpp \
	system/core/libcutils/qtaguid.cpp \
	system/core/libcutils/trace-dev.cpp \
	system/core/libcutils/uevent.cpp
LOCAL_CFLAGS := \
	-Wno-exit-time-destructors
LOCAL_C_INCLUDES := \
	-Iexternal/libcxx/include \
	$(bionicHeaders) \
	-Isystem/libbase/include \
	-Isystem/core/libcutils/include
LOCAL_EXTRA_DEPS := $(builddir)/libcutils_sockets.a
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libcutils_sockets.a

include static_library.mk

include cleanvars.mk

LOCAL_MODULE := libcutils
LOCAL_EXTRA_DEPS := \
	$(builddir)/libcutils.a \
	$(builddir)/libbase.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/liblog.so
LOCAL_SHARED_LIBRARIES := \
	$(builddir)/libbase.so \
	$(builddir)/libdl.so \
	$(builddir)/libc.so \
	$(builddir)/libm.so \
	$(builddir)/libc++.so \
	$(builddir)/liblog.so
LOCAL_WHOLE_STATIC_LIBRARIES := $(builddir)/libcutils.a

include shared_library.mk
