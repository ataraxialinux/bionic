include defines.mk

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share
INCLUDEDIR ?= $(PREFIX)/include
LIBDIR ?= $(PREFIX)/lib

include crt.mk

include libscudo.mk
include libpropertyinfoparser.mk
include arm-optimized-routines.mk
include gwp_asan.mk

include libdl.mk
include libc.mk
include libm.mk

include libcxxabi.mk
include libcxx.mk

include liblog.mk
include libbase.mk
include libcutils.mk

include icu.mk

include libz.mk
include libziparchive.mk

include libjsoncpp.mk

include libcap.mk
include libsysutils.mk
include libpackagelistparser.mk
include libprocessgroup.mk
include libzstd.mk

include logd.mk
include linker.mk

include sources.mk

SUBDIRS = \
	build \
	bionic \
	external/arm-optimized-routines \
	external/googletest \
	external/gwp_asan \
	external/icu \
	external/jsoncpp \
	external/libcap \
	external/libcxx \
	external/libcxxabi \
	external/scudo \
	external/zlib \
	external/zstd \
	system/core \
	system/libbase \
	system/libziparchive \
	system/logging \
	system/timezone

download: $(SUBDIRS)

clean: out
	@echo CLEAN
	@rm -rf out $(SUBDIRS)

install: install-headers install-crt install-static-lib install-dynamic-lib install-executables install-tz

install-headers:
	@echo INSTALLING HEADERS
	@mkdir -p $(DESTDIR)$(INCLUDEDIR)/$(asmTarget)
	@mkdir -p $(DESTDIR)$(INCLUDEDIR)/c++/v1

	@cp -a bionic/libc/include/* $(DESTDIR)$(INCLUDEDIR)

	@cp -a bionic/libc/kernel/uapi/asm-generic $(DESTDIR)$(INCLUDEDIR)/asm-generic
	@cp -a bionic/libc/kernel/uapi/drm $(DESTDIR)$(INCLUDEDIR)/drm
	@cp -a bionic/libc/kernel/uapi/linux $(DESTDIR)$(INCLUDEDIR)/linux
	@cp -a bionic/libc/kernel/uapi/misc $(DESTDIR)$(INCLUDEDIR)/misc
	@cp -a bionic/libc/kernel/uapi/mtd $(DESTDIR)$(INCLUDEDIR)/mtd
	@cp -a bionic/libc/kernel/uapi/rdma $(DESTDIR)$(INCLUDEDIR)/rdma
	@cp -a bionic/libc/kernel/uapi/scsi $(DESTDIR)$(INCLUDEDIR)/scsi
	@cp -a bionic/libc/kernel/uapi/sound $(DESTDIR)$(INCLUDEDIR)/sound
	@cp -a bionic/libc/kernel/uapi/video $(DESTDIR)$(INCLUDEDIR)/video
	@cp -a bionic/libc/kernel/uapi/xen $(DESTDIR)$(INCLUDEDIR)/xen

	@cp -a bionic/libc/kernel/android/uapi/linux/*.h $(DESTDIR)$(INCLUDEDIR)/linux
	@cp -a bionic/libc/kernel/android/scsi/scsi/*.h $(DESTDIR)$(INCLUDEDIR)/scsi

	@cp -a bionic/libc/kernel/uapi/asm-$(uapiArch)/* $(DESTDIR)$(INCLUDEDIR)/$(asmTarget)

	@rm -rf $(DESTDIR)$(INCLUDEDIR)/c++/v1/__cxxabi_config.h

	@cp -a external/libcxx/include/* $(DESTDIR)$(INCLUDEDIR)/c++/v1

	@rm -rf $(DESTDIR)$(INCLUDEDIR)/c++/v1/__cxxabi_config.h

	@cp -a external/libcxxabi/include/* $(DESTDIR)$(INCLUDEDIR)/c++/v1

	@cp -a external/zlib/zconf.h $(DESTDIR)$(INCLUDEDIR)
	@cp -a external/zlib/zlib.h $(DESTDIR)$(INCLUDEDIR)

	@cp -a system/logging/liblog/include/android/* $(DESTDIR)$(INCLUDEDIR)/android

	@cp -a external/libcap/libcap/include/sys/* $(DESTDIR)$(INCLUDEDIR)/sys

install-crt:
	@echo INSTALLING C RUNTIME OBJECTS
	@mkdir -p $(DESTDIR)$(LIBDIR)

	@cp $(builddir)/crtbegin_dynamic.o $(DESTDIR)$(LIBDIR)/crtbegin_dynamic.o
	@cp $(builddir)/crtbegin_so.o $(DESTDIR)$(LIBDIR)/crtbegin_so.o
	@cp $(builddir)/crtbegin_static.o $(DESTDIR)$(LIBDIR)/crtbegin_static.o
	@cp $(builddir)/crtend_android.o $(DESTDIR)$(LIBDIR)/crtend_android.o
	@cp $(builddir)/crtend_so.o $(DESTDIR)$(LIBDIR)/crtend_so.o

install-static-lib:
	@echo INSTALLING STATIC LIBRARIES
	@mkdir -p $(DESTDIR)$(LIBDIR)

	@cp $(builddir)/libdl.a $(DESTDIR)$(LIBDIR)/libdl.a
	@cp $(builddir)/libc.a $(DESTDIR)$(LIBDIR)/libc.a
	@cp $(builddir)/libstdc++.a $(DESTDIR)$(LIBDIR)/libstdc++.a
	@cp $(builddir)/libm.a $(DESTDIR)$(LIBDIR)/libm.a
	@cp $(builddir)/libc++.a $(DESTDIR)$(LIBDIR)/libc++.a
	@cp $(builddir)/libc++experimental.a $(DESTDIR)$(LIBDIR)/libc++experimental.a
	@cp $(builddir)/libc++demangle.a $(DESTDIR)$(LIBDIR)/libc++demangle.a
	@cp $(builddir)/libc++fs.a $(DESTDIR)$(LIBDIR)/libc++fs.a
	@cp $(builddir)/libz.a $(DESTDIR)$(LIBDIR)/libz.a
	@cp $(builddir)/libziparchive.a $(DESTDIR)$(LIBDIR)/libziparchive.a
	@cp $(builddir)/libbase.a $(DESTDIR)$(LIBDIR)/libbase.a
	@cp $(builddir)/liblog.a $(DESTDIR)$(LIBDIR)/liblog.a
	@cp $(builddir)/libcutils.a $(DESTDIR)$(LIBDIR)/libcutils.a
	@cp $(builddir)/libcap.a $(DESTDIR)$(LIBDIR)/libcap.a
	@cp $(builddir)/libicuuc.a $(DESTDIR)$(LIBDIR)/libicuuc.a
	@cp $(builddir)/libicui18n.a $(DESTDIR)$(LIBDIR)/libicui18n.a

install-dynamic-lib:
	@echo INSTALLING DYNAMIC LIBRARIES
	@mkdir -p $(DESTDIR)$(LIBDIR)

	@cp $(builddir)/ld-android.so $(DESTDIR)$(LIBDIR)/ld-android.so
	@cp $(builddir)/libdl_android.so $(DESTDIR)$(LIBDIR)/libdl_android.so
	@cp $(builddir)/libdl.so $(DESTDIR)$(LIBDIR)/libdl.so
	@cp $(builddir)/libc.so $(DESTDIR)$(LIBDIR)/libc.so
	@cp $(builddir)/libstdc++.so $(DESTDIR)$(LIBDIR)/libstdc++.so
	@cp $(builddir)/libm.so $(DESTDIR)$(LIBDIR)/libm.so
	@cp $(builddir)/libc++.so $(DESTDIR)$(LIBDIR)/libc++.so
	@cp $(builddir)/libz.so $(DESTDIR)$(LIBDIR)/libz.so
	@cp $(builddir)/libziparchive.so $(DESTDIR)$(LIBDIR)/libziparchive.so
	@cp $(builddir)/libbase.so $(DESTDIR)$(LIBDIR)/libbase.so
	@cp $(builddir)/liblog.so $(DESTDIR)$(LIBDIR)/liblog.so
	@cp $(builddir)/liblogwrapper.so $(DESTDIR)$(LIBDIR)/liblogwrapper.so
	@cp $(builddir)/libcutils.so $(DESTDIR)$(LIBDIR)/libcutils.so
	@cp $(builddir)/libcap.so $(DESTDIR)$(LIBDIR)/libcap.so
	@cp $(builddir)/libcgrouprc.so $(DESTDIR)$(LIBDIR)/libcgrouprc.so
	@cp $(builddir)/libpackagelistparser.so $(DESTDIR)$(LIBDIR)/libpackagelistparser.so
	@cp $(builddir)/libprocessgroup.so $(DESTDIR)$(LIBDIR)/libprocessgroup.so
	@cp $(builddir)/libsysutils.so $(DESTDIR)$(LIBDIR)/libsysutils.so
	@cp $(builddir)/libicuuc.so $(DESTDIR)$(LIBDIR)/libicuuc.so
	@cp $(builddir)/libicui18n.so $(DESTDIR)$(LIBDIR)/libicui18n.so
	@cp $(builddir)/libicu.so $(DESTDIR)$(LIBDIR)/libicu.so

install-executables:
	@echo INSTALLING EXECUTABLES
	@mkdir -p $(DESTDIR)$(BINDIR)

	@cp $(builddir)/linker_executable/objs/linker $(DESTDIR)$(BINDIR)/$(notdir $(dynamic_linker))
	@cp $(builddir)/logd_executable/objs/logd $(DESTDIR)$(BINDIR)/logd
	@cp $(builddir)/logcat_executable/objs/logcat $(DESTDIR)$(BINDIR)/logcat
	@cp $(builddir)/logwrapper_executable/objs/logwrapper $(DESTDIR)$(BINDIR)/logwrapper
	@cp $(builddir)/getcap_executable/objs/getcap $(DESTDIR)$(BINDIR)/getcap
	@cp $(builddir)/setcap_executable/objs/setcap $(DESTDIR)$(BINDIR)/setcap

install-tz:
	@echo INSTALLING TIMEZONE DATA
	@mkdir -p $(DESTDIR)$(DATADIR)/zoneinfo

	@cp system/timezone/output_data/iana/tzdata $(DESTDIR)$(DATADIR)/zoneinfo/tzdata
	@cp system/timezone/output_data/version/tz_version  $(DESTDIR)$(DATADIR)/zoneinfo/tz_version

archive:
	@tar -czv --exclude .git -f bionic-$(AndroidBranch).tar.gz $(SUBDIRS) $(shell git ls-files)
