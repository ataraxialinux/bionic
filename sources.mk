include defines.mk

build:
	git clone https://android.googlesource.com/platform/build --depth 1 -b $(AndroidBranch) $@

bionic:
	git clone https://android.googlesource.com/platform/bionic --depth 1 -b $(AndroidBranch) $@

external/arm-optimized-routines:
	git clone https://android.googlesource.com/platform/external/arm-optimized-routines --depth 1 -b $(AndroidBranch) $@

external/googletest:
	git clone https://android.googlesource.com/platform/external/googletest --depth 1 -b $(AndroidBranch) $@

external/gwp_asan:
	git clone https://android.googlesource.com/platform/external/gwp_asan --depth 1 -b $(AndroidBranch) $@

external/icu:
	git clone https://android.googlesource.com/platform/external/icu --depth 1 -b $(AndroidBranch) $@

external/jsoncpp:
	git clone https://android.googlesource.com/platform/external/jsoncpp --depth 1 -b $(AndroidBranch) $@

external/libcap:
	git clone https://android.googlesource.com/platform/external/libcap --depth 1 -b $(AndroidBranch) $@

external/libcxx:
	git clone https://android.googlesource.com/platform/external/libcxx --depth 1 -b $(AndroidBranch) $@

external/libcxxabi:
	git clone https://android.googlesource.com/platform/external/libcxxabi --depth 1 -b $(AndroidBranch) $@

external/scudo:
	git clone https://android.googlesource.com/platform/external/scudo --depth 1 -b $(AndroidBranch) $@

external/zlib:
	git clone https://android.googlesource.com/platform/external/zlib --depth 1 -b $(AndroidBranch) $@

external/zstd:
	git clone https://android.googlesource.com/platform/external/zstd --depth 1 -b $(AndroidBranch) $@

system/core:
	git clone https://android.googlesource.com/platform/system/core --depth 1 -b $(AndroidBranch) $@

system/libbase:
	git clone https://android.googlesource.com/platform/system/libbase --depth 1 -b $(AndroidBranch) $@

system/libziparchive:
	git clone https://android.googlesource.com/platform/system/libziparchive --depth 1 -b $(AndroidBranch) $@

system/logging:
	git clone https://android.googlesource.com/platform/system/logging --depth 1 -b $(AndroidBranch) $@

system/timezone:
	git clone https://android.googlesource.com/platform/system/timezone --depth 1 -b $(AndroidBranch) $@

