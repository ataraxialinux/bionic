include defines.mk

include cleanvars.mk

LOCAL_MODULE := libpropertyinfoparser
LOCAL_SRC_FILES := system/core/property_service/libpropertyinfoparser/property_info_parser.cpp
LOCAL_CPPFLAGS := \
	-Wall \
	-Wextra \
	-Werror
LOCAL_C_INCLUDES := \
	$(bionicHeaders) \
	-Isystem/core/property_service/libpropertyinfoparser/include
LOCAL_MODULE_NO_STL_STD := true

include static_library.mk
